/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.content.Context;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class UtilitiesUnitTests {
    // ====================== isNumeric() ==========================================================
    @Test
    public void isNumeric_isCorrect1() throws Exception {
        assertEquals(true, Utilities.isNumeric("5"));
    }
    @Test
    public void isNumeric_isCorrect2() throws Exception {
        assertEquals(true, Utilities.isNumeric("2147483647")); // Int MAX
    }
    @Test
    public void isNumeric_isCorrect3() throws Exception {
        assertEquals(true, Utilities.isNumeric("2147483648")); // Int MAX + 1 (this should still be numeric)
    }
    @Test
    public void isNumeric_isIncorrect1() throws Exception {
        assertEquals(false, Utilities.isNumeric("a"));
    }
    @Test
    public void isNumeric_isIncorrect2() throws Exception {
        assertEquals(false, Utilities.isNumeric("2a"));
    }
    @Test
    public void isNumeric_isIncorrect3() throws Exception {
        assertEquals(false, Utilities.isNumeric("!@#$%^&*()"));
    }

    // ====================== isUserInRangeForPickup() ============================================
    @Test
    public void isUserInRangeForPickup_isCorrect1() throws Exception {
        assertEquals(true, Utilities.isUserInRangeForPickup(-33.831994, 18.697273, -33.831550, 18.697318)); // 50m
    }
    @Test
    public void isUserInRangeForPickup_isCorrect2() throws Exception {
        assertEquals(true, Utilities.isUserInRangeForPickup(-33.831994, 18.697273, -33.831556, 18.697316)); // 49m
    }
    @Test
    public void isUserInRangeForPickup_isIncorrect1() throws Exception {
        assertEquals(false, Utilities.isUserInRangeForPickup(-33.831994, 18.697273, -33.831541, 18.697318)); // 51m
    }

    // ====================== isUserInRangeForDropOff() ============================================
    @Test
    public void isUserInRangeForDropOff_isCorrect1() throws Exception {
        assertEquals(true, Utilities.isUserInRangeForDropOff(-33.831994, 18.697273, -33.827519, 18.697754)); // 500m
    }
    @Test
    public void isUserInRangeForDropOff_isCorrect2() throws Exception {
        assertEquals(true, Utilities.isUserInRangeForDropOff(-33.831994, 18.697273, -33.827529, 18.697754)); // 499m
    }
    @Test
    public void isUserInRangeForDropOff_isIncorrect1() throws Exception {
        assertEquals(false, Utilities.isUserInRangeForDropOff(-33.831994, 18.697273, -33.827511, 18.697754)); // 501m
    }

    // ====================== UserDetails ==========================================================
    private Utilities.UserDetails userDetails;
    @Before
    public void setUp() {
        // Set up a user for the test, manipulating instance variables
        userDetails = new Utilities.UserDetails();
        userDetails.setName("bob");
        userDetails.setRating(5);
        userDetails.setImageURL("http://afakeurl.com");
        userDetails.setDepartureTime("16:20");
        userDetails.setSID("sid");
        userDetails.setVID("vid");
        userDetails.setAID("aid");
        userDetails.setOriginLat(1);
        userDetails.setOriginLon(2);
        userDetails.setDestinationLat(3);
        userDetails.setDestinationLon(4);
        userDetails.setIsDriver(true);
        userDetails.setIsPassenger(false);
    }

    @Test
    public void getName_isCorrect() throws Exception {
        assertEquals("bob", userDetails.getName());
    }
    @Test
    public void getRating_isCorrect() throws Exception {
        assertEquals(5, userDetails.getRating(), 0); // 0 delta <- must be exactly the same
    }
    @Test
    public void getImageURL_isCorrect() throws Exception {
        assertEquals("http://afakeurl.com", userDetails.getImageURL());
    }
    @Test
    public void getDepartureTime_isCorrect() throws Exception {
        assertEquals("16:20", userDetails.getDepartureTime());
    }
    @Test
    public void getSID_isCorrect() throws Exception {
        assertEquals("sid", userDetails.getSID());
    }
    @Test
    public void getAID_isCorrect() throws Exception {
        assertEquals("aid", userDetails.getAID());
    }
    @Test
    public void getVID_isCorrect() throws Exception {
        assertEquals("vid", userDetails.getVID());
    }
    @Test
    public void getOriginLat_isCorrect() throws Exception {
        assertEquals(1, userDetails.getOriginLat(), 0); // 0 delta <- must be exactly the same
    }
    @Test
    public void getOriginLon_isCorrect() throws Exception {
        assertEquals(2, userDetails.getOriginLon(), 0); // 0 delta <- must be exactly the same
    }
    @Test
    public void getDestinationLat_isCorrect() throws Exception {
        assertEquals(3, userDetails.getDestinationLat(), 0); // 0 delta <- must be exactly the same
    }
    @Test
    public void getDestinationLon_isCorrect() throws Exception {
        assertEquals(4, userDetails.getDestinationLon(), 0); // 0 delta <- must be exactly the same
    }
    @Test
    public void getIsDriver_isCorrect() throws Exception {
        assertTrue(userDetails.getIsDriver());
    }
    @Test
    public void getIsPassenger_isCorrect() throws Exception {
        assertFalse(userDetails.getIsPassenger());
    }

    @After
    public void tearDown() {
        userDetails = null;
    }

    // ====================== validateTripInfo() ===================================================
    private Context context;

    @Test
    public void validateTripInfo_isCorrect() throws Exception {
        assertTrue(Utilities.validateTripInfo(context, 1, 2));
    }
}