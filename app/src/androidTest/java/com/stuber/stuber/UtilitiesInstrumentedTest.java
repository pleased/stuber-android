/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class UtilitiesInstrumentedTest {
    // Context of the app under test.
    private Context context = InstrumentationRegistry.getTargetContext();
    @Test
    public void useAppContext() throws Exception {

        assertEquals("com.stuber.stuber", context.getPackageName());
    }

    @Test
    public void validateTripInfo_isCorrect() throws Exception {
        assertTrue(Utilities.validateTripInfo(context,1,2));
    }
}
