/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

class PendingRideRequestsAdaptor extends ArrayAdapter<String> {

    // This is future work

    public PendingRideRequestsAdaptor(Context context, String[] driverNames) {
        super(context, R.layout.pending_ride_request_row, driverNames);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater recommendedDriversInflator = LayoutInflater.from(getContext());
        View recommendedDriversView = recommendedDriversInflator.inflate(R.layout.pending_ride_request_row, parent, false);

        String driverItem = getItem(position);
        TextView recommendedDriverName = (TextView) recommendedDriversView.findViewById(R.id.textViewRecommendedDriverName);
        ImageView recommendedDriverProfilePic = (ImageView) recommendedDriversView.findViewById(R.id.recommnededDriverProfilePic);

        // Set the text of the drivers names and set the profile pic
        recommendedDriverName.setText(driverItem);
        recommendedDriverProfilePic.setImageResource(R.drawable.car);

        return recommendedDriversView;
    }
}
