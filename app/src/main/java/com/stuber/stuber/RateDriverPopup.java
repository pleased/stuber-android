/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

public class RateDriverPopup extends Activity implements View.OnClickListener {
    private Button acceptButton;
    private RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_rate_driver);

        // Initialise objects that are linked to items in the UI such as buttons, text, etc.
        acceptButton = (Button)findViewById(R.id.btn_accept_rate_driver);
        ratingBar = (RatingBar)findViewById(R.id.ratingBar);

        // Set up action listeners for buttons
        acceptButton.setOnClickListener(this);

        // Obtain the device's resolution and create the popup size relative to that
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        // Scale the size of the popup to be smaller than the full screen
        getWindow().setLayout((int)(width*0.8), (int)(height*0.6));

        Drawable progress = ratingBar.getProgressDrawable();
        DrawableCompat.setTint(progress, Color.YELLOW);
    }

    @Override
    public void onClick(View v) {
        // Check which button got pressed
        switch (v.getId()) {
            case R.id.btn_accept_rate_driver:
                int driverRating = ratingBar.getProgress();
                RestCalls r = new RestCalls();
                r.rateDriver(this, driverRating);

                Toast.makeText(getBaseContext(), getString(R.string.driver_rated) + driverRating + "/5", Toast.LENGTH_SHORT).show();
                // Close the popup
                this.finish();
                break;
            case R.id.btn_decline_rate_driver:
                // Close the popup
                this.finish();
                break;
        }
    }
}
