/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;

public class RegisterVehiclePopup extends Activity implements View.OnClickListener {
    private Button acceptButton;
    private Button declineButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_register_vehicle_request);

        // Initialise objects that are linked to items in the UI such as buttons, text, etc.
        acceptButton = (Button)findViewById(R.id.btn_accept_register_vehicle);
        declineButton = (Button)findViewById(R.id.btn_decline_register_vehicle);

        // Set up action listeners for buttons
        acceptButton.setOnClickListener(this);
        declineButton.setOnClickListener(this);


        // Obtain the device's resolution and create the popup size relative to that
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        // Scale the popup to be smaller than the full screen
        getWindow().setLayout((int)(width*0.8), (int)(height*0.6));
    }

    @Override
    public void onClick(View v) {
        // Check which button got pressed
        switch (v.getId()) {
            case R.id.btn_accept_register_vehicle:
                Intent intent = new Intent(this, SignUpToDriveActivity.class);
                startActivity(intent);
                // Close the popup
                this.finish();
                break;

            case R.id.btn_decline_register_vehicle:
                // Close the popup
                this.finish();
                break;
        }
    }
}
