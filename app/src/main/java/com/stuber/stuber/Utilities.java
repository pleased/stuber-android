/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.InputStream;

public class Utilities {

    // A class used to load the profile picture from a URL
    public static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    /**
     * A RecommendedDriver is an object used to contain all the necessary details pertaining to a driver
     * that a user might select as their driver.
     */
    // requests a driver for a lift
    public static class UserDetails {
        private String name;
        private double rating;
        private String imgURL;
        private String departureTime;
        private String sID;
        private String vID;
        private String aID;
        private double originLat;
        private double originLon;
        private double destinationLat;
        private double destinationLon;
        private boolean isDriver;
        private boolean isPassenger;


        // Set methods
        public void setName(String n) { name = n; }
        public void setRating(double r) { rating = r; }
        public void setImageURL(String url) { imgURL = url; }
        public void setDepartureTime(String time) { departureTime = time; }
        public void setSID(String sid) { sID = sid; }
        public void setVID(String vid) { vID = vid; }
        public void setAID(String aid) { aID = aid; }
        public void setOriginLat(double lat) { originLat = lat; }
        public void setOriginLon(double lon) { originLon = lon; }
        public void setDestinationLat(double lat) { destinationLat = lat; }
        public void setDestinationLon(double lon) { destinationLon = lon; }
        public void setIsDriver(boolean isd) { isDriver = isd; }
        public void setIsPassenger(boolean isp) { isPassenger = isp; }

        // Get methods
        public String getName() { return name; }
        public double getRating() { return rating; }
        public String getImageURL() { return imgURL; }
        public String getDepartureTime() { return departureTime; }
        public String getSID() { return sID; }
        public String getVID() { return vID; }
        public String getAID() { return aID; }
        public double getOriginLat() { return originLat; }
        public double getOriginLon() { return originLon; }
        public double getDestinationLat() { return destinationLat; }
        public double getDestinationLon() { return destinationLon; }
        public boolean getIsDriver() { return isDriver; }
        public boolean getIsPassenger() { return isPassenger; }

    }

    /**
     * A function that checks the distance of the user and their destination and if they are within
     * 500m returns true.
     * @param userLat User's latitude
     * @param userLon User's longitude
     * @param destinationLat Destination Latitude
     * @param destinationLon Destination Longitude
     * @return True if the user is within 500 meters of their destination
     */
    public static boolean isUserInRangeForDropOff(double userLat, double userLon, double destinationLat, double destinationLon) {
        return (distFrom(userLat, userLon, destinationLat, destinationLon) <= 500); // Within 500 meters
    }

    /**
     * A function that checks the distance of the driver from the passenger and if it within
     * 50 meters, returns True.
     * @param driverLat Driver's latitude
     * @param driverLon Driver's longitude
     * @param passengerLat Passenger's Latitude
     * @param passengerLon Passenger's Longitude
     * @return True if the driver is within 50 meters of the passenger
     */
    public static boolean isUserInRangeForPickup(double driverLat, double driverLon, double passengerLat, double passengerLon) {
        return (distFrom(driverLat, driverLon, passengerLat, passengerLon) <= 50); // Within 50 meters
    }

    /**
     * A function that given 2 sets of lat-lon pairs calculates the distance in meters.
     * NOTE: This was taken directly from: https://stackoverflow.com/questions/837872/calculate-distance-in-meters-when-you-know-longitude-and-latitude-in-java
     * @param lat1 Latitude of location 1
     * @param lng1 Longitude of location 1
     * @param lat2 Latitude of location 2
     * @param lng2 Longitude of location 2
     * @return
     */
    public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist = earthRadius * c;
        return dist;
    }

    /**
     * A simple function to tell if a String is an integer or not
     * @param s The string to be tested
     * @return True if the String is an integer, False otherwise
     */
    public static boolean isNumeric(String s) {
        return s != null && s.matches("\\d+");
    }

    /**
     * A method used to hide the user's keyboard, normally uesd once the user has executed an action
     * and we wish to display a Toast
     * @param activity The calling activity
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) view = new View(activity);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Used to validate user input for registration of a vehicle.
     * For inout to be considered valid, each entry must be filled in (including a vehicle image)
     * and the number of seats specified must be a positive integer >= 1.
     * Checking for duplicate registration numbers is done in RestCalls.java
     * @param context The context of the calling activity
     * @param vehicleImage The bitmap of the image the user selected
     * @param model The model of the vehicle
     * @param make The make of the vehicle
     * @param colour The colour of the vehicle
     * @param registrationNumber The registration number of the vehicle
     * @param numberOfSeatsString The total number of seats available in the vehicle
     * @return true if all input is valid.
     */
    public static boolean validateVehicle(Context context, Bitmap vehicleImage, String model, String make, String colour, String registrationNumber, String numberOfSeatsString ) {
        boolean success = false;

        // Trim all strings
        model = model.trim();
        make = make.trim();
        colour = colour.trim();
        registrationNumber = registrationNumber.trim();

        if (vehicleImage == null || model.equals("") || make.equals("") || colour.equals("") || registrationNumber.equals("") || numberOfSeatsString.equals("")) {
            Toast.makeText(context, R.string.please_enter_all_vehicle_details, Toast.LENGTH_LONG).show();
        } else {
            if (Utilities.isNumeric(numberOfSeatsString)) {
                // Ensure the number of seats is less than INTEGER_MAX
                try {
                    int numberOfSeats = Integer.parseInt(numberOfSeatsString);
                    if (numberOfSeats >= 2) {
                        return true;
                    } else {
                        Toast.makeText(context, R.string.need_two_or_more_seats, Toast.LENGTH_LONG).show();
                    }
                } catch (java.lang.NumberFormatException e) {
                    Toast.makeText(context, R.string.num_seats_too_high, Toast.LENGTH_LONG).show();
                }
            } else { // If the user has entered something that isn't a number for number of seats
                Toast.makeText(context, R.string.enter_positive_number_of_seats, Toast.LENGTH_LONG).show();
            }
        }

        return success;
    }

    /**
     * Used to validate user input for creating or requesting a lift
     * @param lon The longitude of the destination address
     * @param lat The latitude of the destination address
     * @return true if the user has entered their destination address
     */
    public static boolean validateTripInfo(Context context, double lon, double lat) {
        if (lon != 0 && lat != 0) { // Ensure that the user's destination has been filled in
            return true;
        }
        Toast.makeText(context, R.string.please_set_destination, Toast.LENGTH_LONG).show();
        return false;
    }

}
