/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class RecommendedDriversActivity extends Activity {

    String[] recommendedDriversNames;
    ListAdapter recommendedDriversAdaptor;
    private TextView driverNameTextView;
    private TextView departureTimeTextVeiw;
    private TextView ratingTextView;
    private TextView driverMatchesNotificationTextView;
    public static Activity recommendedDriversActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommended_drivers);

        // Get references to the UI elements
        driverNameTextView = (TextView)findViewById(R.id.textViewDriverName);
        departureTimeTextVeiw = (TextView)findViewById(R.id.textViewDepartureTime);
        ratingTextView = (TextView)findViewById(R.id.textViewRating);
        driverMatchesNotificationTextView = (TextView)findViewById(R.id.txtViewDriverMatchesNotification);

        recommendedDriversNames = new String[]{""};
        recommendedDriversAdaptor = new RecommendedDriversAdaptor(this, recommendedDriversNames);

        // Stop the screen from dimming
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Execute the LoadRecommendedDriversAsyncTask below
        new LoadRecommendedDriversAsyncTask().execute();

        // Give a public reference to this activity so that the driver info activity can close it
        recommendedDriversActivity = this;
    }

    /**
     * Takes the user to the driver info activity with the driverSID, vID and aID
     * @param driverSID the driver's sID (Stuber ID)
     * @param vID the driver's vID (vehicle ID)
     * @param aID the active ID (the ID of the trip)
     */
    public void goToDriverInfoActivity(String driverSID, String vID, String aID) {
        Intent intent = new Intent(this, DriverInfoActivity.class);
        intent.putExtra("driverSID", driverSID);
        intent.putExtra("vID", vID);
        intent.putExtra("aID", aID);
        startActivity(intent);
    }

    private class LoadRecommendedDriversAsyncTask extends AsyncTask<Void, Void, String[]> {

        @Override
        protected void onPreExecute() {
            // start loading animation maybe?
            driverNameTextView.setVisibility(View.INVISIBLE);
            departureTimeTextVeiw.setVisibility(View.INVISIBLE);
            ratingTextView.setVisibility(View.INVISIBLE);
            driverMatchesNotificationTextView.setText(R.string.looking_for_drivers);
        }

        @Override
        protected String[] doInBackground(Void... params) {
            // Obtain the intent extras that will be used to create the rest call to request a trip
            // Note, any second arguments below are the values it will default to if it's not found
            getIntent().getDoubleExtra("pickupLat", 0);
            double pickupLat = getIntent().getDoubleExtra("pickupLat",0);
            double pickupLon = getIntent().getDoubleExtra("pickupLon",0);
            double destinationLat = getIntent().getDoubleExtra("destinationLat",0);
            double destinationLon = getIntent().getDoubleExtra("destinationLon",0);
            String pickupTime = getIntent().getStringExtra("pickupTime");
            String arriveTime = getIntent().getStringExtra("arriveTime");
            String pickupDate = getIntent().getStringExtra("pickupDate");
            String[] daysOfWeek = getIntent().getStringArrayExtra("daysOfWeek");
            boolean instant = getIntent().getBooleanExtra("instant", false);

            RestCalls r = new RestCalls();
            Utilities.UserDetails[] recommendedDrivers = r.requestTrip(RecommendedDriversActivity.this, pickupLat, pickupLon, destinationLat, destinationLon, pickupTime.split(":"), arriveTime.split(":"), pickupDate, daysOfWeek, instant);

            // Build up an array of the potential drivers details, 1 entry = 1 driver with details
            // separated by commas
            if (recommendedDrivers != null) {
                String[] driversArr = new String[recommendedDrivers.length];
                for (int i = 0; i < recommendedDrivers.length; i++) {
                    driversArr[i] = recommendedDrivers[i].getName() + "," + recommendedDrivers[i].getDepartureTime() + "," + recommendedDrivers[i].getRating() + ",http://52.38.242.114:3000/api/v1/user/" + recommendedDrivers[i].getSID() +"/image" + /*recommendedDrivers[i].getImageURL() +*/ "," + recommendedDrivers[i].getSID() + "," + recommendedDrivers[i].getVID() + "," + recommendedDrivers[i].getAID();
                }
                return driversArr;
            }
            return null; // If there were no lift matches, return null and handle it in onPostExecute
        }

        @Override
        protected void onPostExecute(String[] drivers) {
            // stop the loading animation or something

            if (drivers != null) {
                // Display the headings
                driverNameTextView.setVisibility(View.VISIBLE);
                departureTimeTextVeiw.setVisibility(View.VISIBLE);
                ratingTextView.setVisibility(View.VISIBLE);
                driverMatchesNotificationTextView.setVisibility(View.INVISIBLE);

                // An adapter that takes the information in the arrays above and puts them into list items.
                ListAdapter recommendedDriversAdaptor = new RecommendedDriversAdaptor(RecommendedDriversActivity.this, drivers);

                // Create references to the UI elements
                ListView recommendedDriversListView = (ListView) findViewById(R.id.listView_recommended_drivers);

                // Set the list view of recommended drivers to use the adaptor created above
                recommendedDriversListView.setAdapter(recommendedDriversAdaptor);

                // Setting up the action listener for the list items
                recommendedDriversListView.setOnItemClickListener(
                        new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                String[] selectedDriverDetails = String.valueOf(parent.getItemAtPosition(position)).split(",");
                                String selectedDriverSID = selectedDriverDetails[selectedDriverDetails.length - 3];
                                String selectedDriverVID = selectedDriverDetails[selectedDriverDetails.length - 2];
                                String selectedDriverAID = selectedDriverDetails[selectedDriverDetails.length - 1];

                                goToDriverInfoActivity(selectedDriverSID, selectedDriverVID, selectedDriverAID);
                            }
                        }

                );
                recommendedDriversAdaptor = new RecommendedDriversAdaptor(RecommendedDriversActivity.this, drivers);
            } else {
                // Let the user know there were no matches
                driverMatchesNotificationTextView.setText(R.string.no_drivers_found);
            }
        }
    }
}
