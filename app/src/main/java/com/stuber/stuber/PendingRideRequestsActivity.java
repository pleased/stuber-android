/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class PendingRideRequestsActivity extends AppCompatActivity {

    // This is future work

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_ride_requests);

        String[] pendingPassengerNames = {"Ross", "Johann", "Steve", "Some Bloke", "Some Bloke", "Some Bloke", "Some Bloke", "Some Bloke"};

        // An adapter that takes the information in the arrays above and puts them into list items.
        ListAdapter recommendedDriversAdaptor = new PendingRideRequestsAdaptor(this, pendingPassengerNames);

        // Create references to the UI elements
        ListView recommendedDriversListView = (ListView) findViewById(R.id.listView_pending_ride_requests);

        // Set the list view of recommended drivers to use the adaptor created above
        recommendedDriversListView.setAdapter(recommendedDriversAdaptor);

        // Setting up the action listener for the list items
        recommendedDriversListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String selectedDriverName = String.valueOf(parent.getItemAtPosition(position));

                        Toast.makeText(PendingRideRequestsActivity.this, selectedDriverName, Toast.LENGTH_LONG).show();
                    }
                }

        );

        // Stop the screen from dimming
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
}
