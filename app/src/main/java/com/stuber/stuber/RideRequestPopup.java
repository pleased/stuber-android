/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class RideRequestPopup extends Activity implements View.OnClickListener {
    private TextView passengersNameTxtView;
    private TextView passengersOriginTxtView;
    private TextView passengersDestinationTxtView;
    private Button acceptButton;
    private Button declineButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_ride_request);

        // Initialise objects that are linked to items in the UI such as buttons, text, etc.
        passengersNameTxtView = (TextView)findViewById(R.id.txt_view_passenger_name);
        passengersOriginTxtView =(TextView)findViewById(R.id.txt_view_from_address);
        passengersDestinationTxtView =(TextView)findViewById(R.id.txt_view_to_address);
        acceptButton = (Button)findViewById(R.id.btn_accept_passenger_request);
        declineButton = (Button)findViewById(R.id.btn_decline_passenger_request);

        // Set up action listeners for buttons
        acceptButton.setOnClickListener(this);
        declineButton.setOnClickListener(this);


        // Obtain the device's resolution and create the popup size relative to that
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*0.8), (int)(height*0.6));

        // Show the user the requesting passengers name, origin and destination
        passengersNameTxtView.setText(getIntent().getStringExtra("requestingPassengerName"));
        passengersOriginTxtView.setText(getIntent().getStringExtra("requestingPassengerOriginAddress"));
        passengersDestinationTxtView.setText(getIntent().getStringExtra("requestingPassengerDestinationAddress"));

        // Play a ping sound to notify the user
        MediaPlayer ping = MediaPlayer.create(this, R.raw.ping);
        ping.start();
    }

    @Override
    public void onClick(View v) {
        // Check which button got pressed
        RestCalls r = new RestCalls();
        switch (v.getId()) {
            case R.id.btn_accept_passenger_request:
                r.tripDecided(this, true, getIntent().getStringExtra("requestingPassengerSID"));
                // Now close the popup
                this.finish();
                break;

            case R.id.btn_decline_passenger_request:
                r.tripDecided(this, false, getIntent().getStringExtra("requestingPassengerSID"));
                this.finish();
                break;
        }
    }
}
