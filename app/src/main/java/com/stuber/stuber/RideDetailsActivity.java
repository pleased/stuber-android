/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import java.util.Calendar;

public class RideDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText numberOfSeatsText;
    private Button findMeADriverButton;
    private Toolbar headerBar;
    private Switch iWillDriveSwitch;
    private NumberPicker numSeatsPicker; // This and anything related to it is for future work
    private TextView passengerLabel, driverLabel;

    // The user's longitude and latitude they have selected for pickup and destination locations.
    static double destinationLat, destinationLon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_details);

        // Initialise objects that are linked to items in the UI such as buttons, text, etc.
        findMeADriverButton = (Button)findViewById(R.id.btn_find_me_a_driver);
        numberOfSeatsText = (EditText)findViewById(R.id.edit_txt_number_of_seats);
        headerBar = (Toolbar)findViewById(R.id.header);
        iWillDriveSwitch = (Switch)findViewById(R.id.switch_i_will_drive);
        numSeatsPicker = (NumberPicker)findViewById(R.id.num_seats_picker);
        passengerLabel = (TextView)findViewById(R.id.txtViewPassengerLabel);
        driverLabel = (TextView)findViewById(R.id.txtViewDriverLabel);

        // Setting up the action listener for the sign in and register buttons
        findMeADriverButton.setOnClickListener(this);
        iWillDriveSwitch.setOnClickListener(this);

        // Set visibility of number of seats field to invisible until the user has said they are a driver
        numberOfSeatsText.setVisibility(View.INVISIBLE);

        driverLabel.setTextColor(Color.GRAY);

        // Set the max and min values of the number of seats number picker
        numSeatsPicker.setMinValue(1); // This is just here so it has values at the start, but will be overridden
        numSeatsPicker.setMaxValue(5);
        numSeatsPicker.setWrapSelectorWheel(false);
        final String[] values = {"5","4", "3", "2", "1"}; // Build up this array from number of seats retrieved from async call
        numSeatsPicker.setDisplayedValues(values);

        // Set up action listener for place selector (Destination)
        PlaceAutocompleteFragment autocompleteFragmentDestination = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_destination);

        autocompleteFragmentDestination.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // Get info about the selected place.
                destinationLat = place.getLatLng().latitude;
                destinationLon = place.getLatLng().longitude;
            }

            @Override
            public void onError(Status status) {
                // Do nothing
            }
        });

        // Change the size of the autocomplete fragment
        ((EditText)autocompleteFragmentDestination.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16.0f);

    }

    @Override
    public void onClick(View v) {
        // Check which button got pressed
        switch (v.getId()) {
            case R.id.btn_find_me_a_driver:
                // Gather the information the user has entered
                String iWillDrive = iWillDriveSwitch.isChecked()+"";
                Calendar rightNow = Calendar.getInstance();
                int hour = rightNow.get(Calendar.HOUR_OF_DAY);
                int minute = rightNow.get(Calendar.MINUTE);
                String pickupTime = hour + ":" + minute;

                // Create an array of the days of the week to be repeated
                String[] daysOfWeek = {"Monday"};

                // Retrieve the current location passed to this activity from the Map activity
                Double originLat = Double.parseDouble(getIntent().getStringExtra("originLat"));
                Double originLon = Double.parseDouble(getIntent().getStringExtra("originLon"));

                if (Utilities.validateTripInfo(getBaseContext(), destinationLon, destinationLat)) { // Validate user input

                    if (iWillDriveSwitch.isChecked()) {
                        // Close the keyboard
                        Utilities.hideKeyboard(this);

                        // If the user is a driver do further validation to ensure the number of seats specified is legal
                        String numberOfSeatsString = numberOfSeatsText.getText().toString().replaceAll("\\s+", ""); // Get the number of seats and remove all whitespace
                        if (Utilities.isNumeric(numberOfSeatsString)) {
                            // Obtain number of seats available (done here because only a driver enters this)
                            int numberOfSeats = Integer.parseInt(numberOfSeatsString);
                            // Hardcoding the user to request to arrive 2 hours from current time when a driver, cos setting a time to arrive
                            // from the front-end perspective is nonsensical
                            SetTripAsyncTask setTripAsyncTask = new SetTripAsyncTask(this, originLat, originLon, destinationLat, destinationLon, pickupTime.split(":"), (hour + 2 + ":" + minute).split(":"), "date_placeholder", numberOfSeats, daysOfWeek, false);
                            setTripAsyncTask.execute();
                        } else {
                            Toast.makeText(getBaseContext(), R.string.set_number_of_seats, Toast.LENGTH_LONG).show();
                        }

                    } else {
                        // Go to the recommended drivers activity
                        Intent intent = new Intent(this, RecommendedDriversActivity.class);
                        // Pass the ride request data onto the next activity so it can make the rest call
                        intent.putExtra("pickupLat", originLat);
                        intent.putExtra("pickupLon", originLon);
                        intent.putExtra("destinationLat", destinationLat);
                        intent.putExtra("destinationLon", destinationLon);
                        intent.putExtra("pickupTime", pickupTime);
                        // Hardcoding the user to request to arrive 2 hours from now when a passenger, cos setting a time to arrive
                        // from the front-end perspective is nonsensical
                        intent.putExtra("arriveTime", (hour + 1 + ":" + minute));
                        intent.putExtra("pickupDate", "date_placeholder");
                        intent.putExtra("daysOfWeek", daysOfWeek);
                        intent.putExtra("instant", false);
                        startActivity(intent);
                        // Close this activity
                        this.finish();
                    }
                }
                break;

            case R.id.switch_i_will_drive:
                // Update the text of the 'find me a driver' button according to whether or not this
                // person is a driver
                if (iWillDriveSwitch.isChecked()) {
                    findMeADriverButton.setText(R.string.set_trip);
                    numberOfSeatsText.setHint(R.string.number_of_seats_available);
                    numberOfSeatsText.setVisibility(View.VISIBLE);
                    passengerLabel.setTextColor(Color.GRAY);
                    driverLabel.setTextColor(Color.BLACK);
                } else {
                    findMeADriverButton.setText(R.string.find_me_a_driver);
                    numberOfSeatsText.setHint(R.string.number_of_seats_needed);
                    numberOfSeatsText.setVisibility(View.INVISIBLE);

                    driverLabel.setTextColor(Color.GRAY);
                    passengerLabel.setTextColor(Color.BLACK);
                }
                break;
        }
    }

    /**
     * An async task used to set a trip.
     */
    private class SetTripAsyncTask extends AsyncTask<Void, Void, String> {
        double pLat, pLon, dLat, dLon;
        String[] sTA, eTA, dOW;
        String d;
        int nOS;
        boolean wr;
        Context c;

        public SetTripAsyncTask(Context context, double pickupLat, double pickupLon, double destinationLat, double destinationLon, String[] startTimeArr, String[] endTimeArr, String date, int numberOfSeats, String[] daysOfWeek, boolean weekRepeat) {
            pLat = pickupLat;
            pLon = pickupLon;
            dLat = destinationLat;
            dLon = destinationLon;
            sTA = startTimeArr;
            eTA = endTimeArr;
            d = date;
            nOS = numberOfSeats;
            dOW = daysOfWeek;
            wr = weekRepeat;
            c = context;
        }

        @Override
        protected void onPreExecute() {
            // start loading animation maybe?
            Toast.makeText(getBaseContext(), R.string.setting_up_trip, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(Void... params) {
            // Obtain the intent extras that will be used to create the rest calls to get drivers and vehicles details
            RestCalls r = new RestCalls();
            return r.setTrip(c, pLat, pLon, dLat, dLon, sTA, eTA, "date_placeholder", nOS, dOW, false);
        }

        @Override
        protected void onPostExecute(String setTripSuccess) {
            // Go back to the map activity
            if (setTripSuccess.equals("true")) {
                RideDetailsActivity.this.finish();
                Toast.makeText(getBaseContext(), R.string.trip_set, Toast.LENGTH_LONG).show();

                // Set the user's status to 'driver' as soon as they have successfully set a trip
                SharedPreferences accountSharedPreferences = getSharedPreferences("accountData", Context.MODE_PRIVATE);
                String email = accountSharedPreferences.getString("email", "Could not get email");
                SharedPreferences statusSharedPref = getSharedPreferences(email, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = statusSharedPref.edit();
                editor.putString(getString(R.string.driver_status), "driver");
                editor.commit();
            } else if (setTripSuccess.equals("noVehicle")) {
                Intent intent = new Intent(getBaseContext(), RegisterVehiclePopup.class);
                startActivity(intent);
            } else if (setTripSuccess.equals("alreadyOnTrip")) {
                Toast.makeText(getBaseContext(), R.string.sorry_already_on_trip, Toast.LENGTH_LONG).show();
                Toast.makeText(getBaseContext(), R.string.sorry_already_on_trip, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getBaseContext(), R.string.not_enough_seats, Toast.LENGTH_LONG).show();
                Toast.makeText(getBaseContext(), R.string.not_enough_seats, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
