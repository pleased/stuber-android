/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;

class RegisteredVehicleAdaptor extends ArrayAdapter<String> {

    public RegisteredVehicleAdaptor(Context context, String[] registeredVehicle) {
        super(context, R.layout.registered_vehicle_row, registeredVehicle);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater recommendedDriversInflator = LayoutInflater.from(getContext());
        View registeredVehicleView = recommendedDriversInflator.inflate(R.layout.registered_vehicle_row, parent, false);

        // Split the driver string above into its separate details and set the text of the
        // relevant text views below
        String[] vehicle = getItem(position).split(",");
        String make = vehicle[0];
        String model = vehicle[1];
        String colour = vehicle[2];
        String maxNumPassengers = vehicle[3];
        String vid = vehicle[4];

        new Utilities.DownloadImageTask((ImageView) registeredVehicleView.findViewById(R.id.imageViewVehicle)).execute("http://52.38.242.114:3000/api/v1/vehicle/" + vid + "/image");
        TextView makeAndModelTextView = (TextView) registeredVehicleView.findViewById(R.id.textViewMakeAndModel);
        TextView colourTextView = (TextView) registeredVehicleView.findViewById(R.id.textViewVehicleColour);
        TextView maxNumPassengersTextView = (TextView) registeredVehicleView.findViewById(R.id.textViewMaxNumPassengers);


        // Set the text of the drivers names and set the profile pic
        makeAndModelTextView.setText(make + " " + model);
        colourTextView.setText(colour);
        maxNumPassengersTextView.setText(maxNumPassengers);

        return registeredVehicleView;
    }
}
