/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;

public class SignUpToDriveActivity extends AppCompatActivity implements View.OnClickListener {

    private Button uploadPictureOfCarFromCameraButton;
    private Button uploadPictureOfCarFromGalleryButton;
    private Button cancelButton;
    private Button registerCarButton;
    private EditText makeText;
    private EditText modelText;
    private EditText colourText;
    private EditText licencePlateNumberText;
    private EditText numberOfSeatsText;
    private static Bitmap vehicleImage;
    private ImageView vehiclePreviewImageView;

    // Variables used to pre-populate fields
    private String make;
    private String model;
    private String colour;
    private String licencePlateNum;
    private String vid;
    private String maxNumPassengers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_to_drive);

        // Initialise objects that are linked to items in the UI such as buttons, text, etc.
        uploadPictureOfCarFromCameraButton = (Button)findViewById(R.id.btn_upload_picture_of_car_from_camera);
        uploadPictureOfCarFromGalleryButton = (Button)findViewById(R.id.btn_upload_picture_of_car_from_gallery);
        cancelButton = (Button)findViewById(R.id.btn_cancel_car_registration);
        registerCarButton = (Button)findViewById(R.id.btn_register_car);
        makeText = (EditText)findViewById(R.id.txt_make);
        modelText = (EditText)findViewById(R.id.txt_model);
        colourText = (EditText)findViewById(R.id.txt_colour);
        licencePlateNumberText = (EditText)findViewById(R.id.txt_licence_plate_number);
        numberOfSeatsText = (EditText)findViewById(R.id.txt_number_of_seats);
        vehiclePreviewImageView = (ImageView)findViewById(R.id.img_view_vehicle_preview);

        // Setting up the action listeners for the buttons
        uploadPictureOfCarFromCameraButton.setOnClickListener(this);
        uploadPictureOfCarFromGalleryButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        registerCarButton.setOnClickListener(this);

        // Set the vehicle preview image to the image set before screen rotation
        if(savedInstanceState != null) {
            Bitmap bitmap = savedInstanceState.getParcelable("vehicle_preview_img");
            vehiclePreviewImageView.setImageBitmap(bitmap);
        } else { // Initialise the car silhouette
            vehiclePreviewImageView.setImageResource(R.drawable.car_silhouette_128);
        }

        make = getIntent().getStringExtra("make");
        model = getIntent().getStringExtra("model");
        colour = getIntent().getStringExtra("colour");
        licencePlateNum = getIntent().getStringExtra("regNum");
        vid = getIntent().getStringExtra("vid");
        maxNumPassengers = getIntent().getStringExtra("maxNumPassengers");

        // If the vehicle's info is initialised fill in the fields
        if (make != null) {
            makeText.setText(make);
            modelText.setText(model);
            colourText.setText(colour);
            licencePlateNumberText.setText(licencePlateNum);
            numberOfSeatsText.setText(maxNumPassengers);

            vehiclePreviewImageView.setImageResource(R.drawable.car_silhouette_loading);
            new Utilities.DownloadImageTask((ImageView) vehiclePreviewImageView.findViewById(R.id.img_view_vehicle_preview)).execute("http://52.38.242.114:3000/api/v1/vehicle/" + vid + "/image");
        }
    }

    @Override
    public void onClick(View v) {
        // Check which button got pressed
        switch (v.getId()) {
            case R.id.btn_upload_picture_of_car_from_camera:
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, 0);//zero can be replaced with any action code
                break;
            case R.id.btn_upload_picture_of_car_from_gallery:
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
                break;
            case R.id.btn_cancel_car_registration:
                this.finish();
                break;
            case R.id.btn_register_car:
                // Initialise the vehicle image from the vehicle image loaded (only if we have pre-loaded the information)
                if (make != null) {
                    vehicleImage = ((BitmapDrawable) vehiclePreviewImageView.getDrawable()).getBitmap();
                }

                // Gathering the user's input
                String model = modelText.getText().toString();
                String make = makeText.getText().toString();
                String colour = colourText.getText().toString();
                String registrationNumber = licencePlateNumberText.getText().toString();
                String numberOfSeatsString = numberOfSeatsText.getText().toString();
                numberOfSeatsString = numberOfSeatsString.replaceAll("\\s+",""); // Remove all whitespace

                // If all input is correct, register the vehicle
                if (Utilities.validateVehicle(getBaseContext(), vehicleImage, model, make, colour, registrationNumber, numberOfSeatsString)) {
                    int numberOfSeats = Integer.parseInt(numberOfSeatsString);
                    RegisterDriverAsyncTask driverAsyncTask = new RegisterDriverAsyncTask(model, make, colour, registrationNumber, numberOfSeats+1, vehicleImage);
                    driverAsyncTask.execute();
                }
                break;
        }
    }

    // onActivityResult runs upon returning to this activity from the gallery or camera
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0: // Camera
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    vehicleImage = null;
                    try {
                        vehicleImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                        vehiclePreviewImageView.setImageBitmap(vehicleImage);

                        // After the camera the app restores in landscape, so change to portrait
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case 1: // Gallery
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    vehicleImage = null;
                    try {
                        vehicleImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                        vehiclePreviewImageView.setImageBitmap(vehicleImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    // Need this so when the user changes the orientation of the device the image of the vehicle remains intact
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        BitmapDrawable drawable = (BitmapDrawable) vehiclePreviewImageView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        outState.putParcelable("vehicle_preview_img", bitmap);
        super.onSaveInstanceState(outState);
    }

    /**
     * An async task used to register a user as a driver
     */
    private class RegisterDriverAsyncTask extends AsyncTask<Void, Void, Boolean> {
        String model, make, colour, registrationNumber;
        int numberOfSeats;
        Bitmap vehicleImage;

        public RegisterDriverAsyncTask(String model, String make, String colour, String registrationNumber, int numberOfSeats, Bitmap vehicleImage) {
            this.model = model;
            this.make = make;
            this.colour = colour;
            this.registrationNumber = registrationNumber;
            this.numberOfSeats = numberOfSeats;
            this.vehicleImage = vehicleImage;
        }

        @Override
        protected void onPreExecute() {
            // Close the keyboard
            Utilities.hideKeyboard(SignUpToDriveActivity.this);

            // Notify the user that their vehicle is being registered
            Toast.makeText(getBaseContext(), R.string.registering_vehicle, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            RestCalls r = new RestCalls();

            // First set the user to a driver
            r.updateDriverStatus(getBaseContext(), true);

            // Then register the vehicle to this driver
            return r.registerVehicle(getBaseContext(), model, make, colour, registrationNumber, numberOfSeats, vehicleImage);
        }

        @Override
        protected void onPostExecute(Boolean registrationSuccess) {
           notifyUserOnRegistrationResult(registrationSuccess);
        }
    }

    public void notifyUserOnRegistrationResult(boolean result) {
        if (result == true) {
            // Go back to the map activity
            this.finish();
            Toast.makeText(getBaseContext(), R.string.vehicle_registered, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getBaseContext(), R.string.number_plate_in_use, Toast.LENGTH_LONG).show();
        }
    }
}
