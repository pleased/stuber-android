/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewPassengerActivity extends AppCompatActivity implements View.OnClickListener {

    // This is future work

    private ImageView profilePictureImageView;
    private TextView nameTextView;
    private TextView pickupAddressTextView;
    private TextView destinationAddressTextView;
    private TextView dateTextView;
    private TextView timeTextView;
    private TextView ratingTextView;
    private Button acceptButton;
    private Button rejectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_passenger);

        // Set up references to UI elements
        profilePictureImageView = (ImageView) findViewById(R.id.imageViewPassengerProfilePicture);
        nameTextView = (TextView) findViewById(R.id.textViewName);
        pickupAddressTextView = (TextView) findViewById(R.id.textViewPickupAddress);
        destinationAddressTextView = (TextView) findViewById(R.id.textViewDestingationAddress);
        dateTextView = (TextView) findViewById(R.id.textViewDate);
        timeTextView = (TextView) findViewById(R.id.textViewTime);
        ratingTextView = (TextView) findViewById(R.id.textViewRating);
        acceptButton = (Button) findViewById(R.id.buttonAccept);
        rejectButton = (Button) findViewById(R.id.buttonReject);

        // Set up the action listeners for the buttons
        acceptButton.setOnClickListener(this);
        rejectButton.setOnClickListener(this);

        // Stop the screen from dimming
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onClick(View v) {
        // Check which button got pressed
        switch (v.getId()) {
            case R.id.buttonAccept:
                break;

            case R.id.buttonReject:
                break;
        }
    }
}
