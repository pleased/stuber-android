/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class RestCalls {

    // Global variables
    private String IP = "52.38.242.114"; // The IP of the server
    private static String token;
    private static String sID;
    private static String vID;
    private static String aID;
    private static String departureTime;
    private static String driverSID;
    private static String drivervID;
    private static String inboxMessage;
    private static String messageType;
    private double latitude, longitude;
    private String setTripSuccess;
    private boolean signinSuccess, registerSuccess, registerVehicleSuccess;
    // String driver and car below are used when retrieving a drivers profile information
    private static String driver, car;
    // index is used for indexing into the recommended drivers array in requestTrip()
    private int index;
    private static String img_url;
    private static Bitmap profileImg, vehicleImg;
    private static Utilities.UserDetails[] recommendedDrivers;
    private int portNumber = 3000;
    // Context below is used for nested rest calls
    private Context c1, c2;

    // The tag used for logging output
    private static final String TAG = "logTag";
    /**
     * registerUser(Context)
     *
     * This method is used to login a user.
     * It will either return success: False in which case the user does not have a registered account or
     * it will return the users SID
     * @param context the context from which this method is being called.
     */
    public boolean registerUser(final Context context, final String GID, String firstName, String email, String[] likes, String[] dislikes, int age, String image_url) {
        img_url = image_url;
        JSONObject j = new JSONObject();
        try {
            j.put("gid", GID);
            j.put("fullname", firstName);
            j.put("age", age);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = null;
        try {
            se = new StringEntity(j.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient sync = new SyncHttpClient();

        sync.post(context, "http://" + IP + ":" + portNumber + "/api/v1/user", se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Register completed successfully!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                // Retrieve the token and store it in global variable 'token'
                try {
                    // Retrieve the token and store it in global variable 'token'
                    JSONObject json = new JSONObject(new String(responseBody));
                    token = json.getJSONObject("data").getString("token");
                    sID = json.getJSONObject("data").getString("sid");
                    Log.i(TAG, "Registration successful!");
                    Log.i(TAG, "Token is: " + token);
                    Log.i(TAG, "sID is: " + sID);
                    registerSuccess = true;

                    // Create a bitmap from the image url to give to setProfileImage rest call
                    class SetProfileImageAsyncTask extends AsyncTask<Void, Void, Bitmap> {

                        @Override
                        protected void onPreExecute() {
                            // Do nothing
                        }

                        @Override
                        protected Bitmap doInBackground(Void... params) {
                            // Create a bitmap from the image url to give to setProfileImage rest call
                            Bitmap image = null;
                            try {
                                URL url = new URL(img_url);
                                image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            } catch(IOException e) {
                            }
                            return image;
                        }

                        @Override
                        protected void onPostExecute(Bitmap profileImage) {
                            // Make the rest call to set the user's profile image
                            setProfileImage(context, profileImage);
                        }
                    }
                    // Start the async task that sets the users profile image
                    new SetProfileImageAsyncTask().execute();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Problem with registration:");
                error.printStackTrace();
                registerSuccess = false;
            }
        });
        return registerSuccess;
    }

    /**
     * login(Context)
     *
     * This method is used to login a user.
     * It will either return success: False in which case the user does not have a registered account or
     * it will return the users SID
     *
     * @param context the context from which this method is being called.
     */
    public boolean login(Context context, String GID) {
        JSONObject j = new JSONObject();

        try {
            j.put("gid", GID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = null;
        try {
            se = new StringEntity(j.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new SyncHttpClient();

        async.post(context, "http://" + IP + ":" + portNumber + "/api/v1/login", se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    // Retrieve the token and store it in global variable 'token'
                    JSONObject json = new JSONObject(new String(responseBody));
                    token = json.getJSONObject("data").getString("token");
                    sID = json.getJSONObject("data").getString("sid");
                    Log.i(TAG, "Sign in successful!");
                    Log.i(TAG, "Token is: " + token);
                    Log.i(TAG, "sID is: " + sID);
                    signinSuccess = true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Could not send GID to the server:");
                error.printStackTrace();
                signinSuccess = false;
            }
        });
        return signinSuccess;
    }

    /**
     * Called if a user wishes to set a trip as a driver
     * @param context The context of the calling activity
     * @param pickupLat The origin latitude of the driver
     * @param pickupLon The origin longitude of the driver
     * @param destinationLat The destination latitude of the driver
     * @param destinationLon The destination longitude of the driver
     * @param startTimeArr The start time of the trip
     * @param endTimeArr The end time of the trip
     * @param date The date the trip is to take place on
     * @param numberOfSeats The number of seats available for this trip
     * @param daysOfWeek Not used
     * @param weekRepeat Not used
     * @return A String letting us know if setting the trip was successful, or if not, what went wrong
     */
    public String setTrip(Context context, final double pickupLat, final double pickupLon, final double destinationLat, final double destinationLon, final String[] startTimeArr, final String[] endTimeArr, String date, final int numberOfSeats, String[] daysOfWeek, boolean weekRepeat) {
        setTripSuccess = "false";
        // No JSON object contains nothing so create StringEntity from the empty String
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new SyncHttpClient();
        async.addHeader("x-access-token", token);

        async.get(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/vehicles", se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (responseBody != null) {
                    Log.i(TAG, "Successfully retrieved VID!");
                    if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));

                    // Extract the VID from the from the response body
                    try {
                        // Retrieve the token and store it in global variable 'token'
                        JSONObject json = new JSONObject(new String(responseBody));
                        JSONArray jsonVehicleArray = json.getJSONArray("data");

                        JSONObject jsonVehicleObject = jsonVehicleArray.getJSONObject(jsonVehicleArray.length()-1);

                        vID = jsonVehicleObject.getString("vid");
                        Log.i(TAG, "VID: " + vID);

                        JSONObject j = new JSONObject();

                        try {
                            JSONObject origin = new JSONObject();
                            origin.put("lat", pickupLat);
                            origin.put("lon", pickupLon);

                            JSONObject currLoc = new JSONObject();
                            currLoc.put("lat", pickupLat);
                            currLoc.put("lon", pickupLon);

                            JSONObject destination = new JSONObject();
                            destination.put("lat", destinationLat);
                            destination.put("lon", destinationLon);

                            JSONObject startTime = new JSONObject();
                            // Note: the server can't handle getting 0 for minute or hour, so
                            // If it is 0, change it to 1
                            startTime.put("hour", Math.max(Integer.parseInt(startTimeArr[0]),1));
                            startTime.put("minute", Math.max(Integer.parseInt(startTimeArr[1]),1));

                            JSONObject endTime = new JSONObject();
                            endTime.put("hour", Math.max(Integer.parseInt(endTimeArr[0]),1));
                            endTime.put("minute", Math.max(Integer.parseInt(endTimeArr[1]),1));

                            j.put("origin", origin);
                            j.put("currLoc", currLoc);
                            j.put("destination", destination);
                            j.put("startTime", startTime);
                            j.put("endTime", endTime);
                            j.put("vid", vID);
                            j.put("openSeats", numberOfSeats);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        StringEntity se = null;
                        try {
                            se = new StringEntity(j.toString());
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                        AsyncHttpClient async = new SyncHttpClient();
                        async.addHeader("x-access-token", token);
                        Log.i(TAG, "TOKEN: " + token);
                        Log.i(TAG, "SID: " + sID);
                        async.post(c1, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/trip", se, "application/json", new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                Log.i(TAG, "Set Trip completed successfully!");
                                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));

                                // Retrieve the aID from the trip just set
                                try {
                                    JSONObject json = new JSONObject(new String(responseBody));
                                    JSONObject jsonObject = json.getJSONObject("data");
                                    aID = jsonObject.getString("aid");
                                    setTripSuccess = "true";
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                Log.i(TAG, "Failed to set trip1:");
                                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
//                                error.printStackTrace();
                                // response body: {"success":false,"message":"Number of open seats specified is more than the vehicle supports"}
                                setTripSuccess = "alreadyOnTrip";

                                // If the vehicle does not have enough seats
                                String response = new String(responseBody);
                                if (response.endsWith("Number of open seats specified is more than the vehicle supports\"}")) {
                                    setTripSuccess = "false";
                                }
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else { // The user said they will drive but they have not yet registered a vehicle
                    setTripSuccess = "noVehicle";
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to set trip2:");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });

        return setTripSuccess;
    }

    /**
     * Called if the user wishes to request a trip as a passenger
     * @param context The context of the calling activity
     * @param pickupLat The origin latitude of the driver
     * @param pickupLon The origin longitude of the driver
     * @param destinationLat The destination latitude of the driver
     * @param destinationLon The destination longitude of the driver
     * @param startTimeArr The start time of the trip
     * @param endTimeArr The end time of the trip
     * @param date The date the trip is to take place on
     * @param daysOfWeek Not used
     * @param weekRepeat Not used
     * @return
     */
    public Utilities.UserDetails[] requestTrip(Context context, double pickupLat, double pickupLon, double destinationLat, double destinationLon, String[] startTimeArr, String[] endTimeArr, String date, String[] daysOfWeek, boolean weekRepeat) {

        JSONObject j = new JSONObject();
        try {
            JSONObject origin = new JSONObject();
            origin.put("lat", pickupLat);
            origin.put("lon", pickupLon);

            JSONObject destination = new JSONObject();
            destination.put("lat", destinationLat);
            destination.put("lon", destinationLon);

            JSONObject startTime = new JSONObject();
            startTime.put("hour", Integer.parseInt(startTimeArr[0]));
            startTime.put("minute", Integer.parseInt(startTimeArr[1]));

            JSONObject endTime = new JSONObject();
            endTime.put("hour", Integer.parseInt(endTimeArr[0]));
            endTime.put("minute", Integer.parseInt(endTimeArr[1]));

            j.put("origin", origin);
            j.put("destination", destination);
            j.put("startTime", startTime);
            j.put("endTime", endTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = null;
        try {
            se = new StringEntity(j.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new SyncHttpClient();
        async.addHeader("x-access-token", token);
        Log.i(TAG, "TOKEN: " + token);
        Log.i(TAG, "SID: " + sID);

        c1 = context;

        async.post(c1, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/lift", se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Request Trip completed successfully!");
                if (responseBody != null) { // Only continue of you get a lift result
                    if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));

                    // Obtain the list of potential lifts/drivers
                    try {
                        JSONObject json = new JSONObject(new String(responseBody));
                        JSONObject jsonObject = json.getJSONObject("data");
                        JSONArray jsonDriverMatchesArray = jsonObject.getJSONArray("matches");

                        // We now know how many driver matches there are, so create an array of RecommendedDriver
                        // objects to store the retrieved data in, to be returned to the activity that
                        // made this rest call.
                        recommendedDrivers = new Utilities.UserDetails[jsonDriverMatchesArray.length()];

                        // Loop through each driver match in the array defined above (first 10)
                        // Then obtain the ride information using the aid
                        for (index = 0; index < jsonDriverMatchesArray.length() && index < 10; index++) {
                            final String aID = jsonDriverMatchesArray.getString(index);

                            // Obtain the ride details for each recommended trip
                            // JSON object contains nothing so create StringEntity from the empty String
                            StringEntity se = null;
                            try {
                                se = new StringEntity("");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                            AsyncHttpClient async = new SyncHttpClient();
                            async.addHeader("x-access-token", token);

                            async.get(c1, "http://" + IP + ":" + portNumber + "/api/v1/trip/" + aID, se, "application/json", new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    Log.i(TAG, "Successfully retrieved trip!");
                                    if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));

                                    // Extract the VID from the from the response body
                                    try {
                                        JSONObject json = new JSONObject(new String(responseBody));
                                        final JSONObject jsonDataObject = json.getJSONObject("data");
                                        JSONObject jsonTripObject = jsonDataObject.getJSONObject("trip");

                                        // Obtain the drivers sid from the jsonTripObject
                                        driverSID = jsonTripObject.getString("sid");

                                        // Obtain the drivers sid and vid to be used in driver info activity
                                        drivervID = jsonTripObject.getString("vid");
                                        departureTime = jsonTripObject.getString("startTime").split("T|\\.|:")[1] + ":" + jsonTripObject.getString("startTime").split("T|\\.|:")[2];
                                        Log.i(TAG, "driverSID: " + driverSID);

                                        // Now request this drivers profile information
                                        // No JSON object contains nothing so create StringEntity from the empty String
                                        StringEntity se = null;
                                        try {
                                            se = new StringEntity("");
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }

                                        AsyncHttpClient async = new SyncHttpClient();
                                        async.addHeader("x-access-token", token);

                                        async.get(c1, "http://" + IP + ":" + portNumber + "/api/v1/user/" + driverSID, se, "application/json", new AsyncHttpResponseHandler() {
                                            @Override
                                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                                Log.i(TAG, "Successfully retrieved users details!");
                                                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));

                                                // Extract the user's name, departure time and rating
                                                try {
                                                    JSONObject json = new JSONObject(new String(responseBody));
                                                    JSONObject jsonUserDataObject = json.getJSONObject("data");

                                                    // Obtain the latest trip from the trip history list
                                                    JSONObject jsonTripObject = jsonUserDataObject.getJSONArray("tripHistory").getJSONObject(0);
                                                    // The time is a long string including year etc, so strip time out
                                                    String startTime = jsonTripObject.getString("startTime");

                                                    // User's name, rating and profile picture url
                                                    String name = jsonUserDataObject.getString("fullname");
                                                    String rating = jsonUserDataObject.getString("rating");
                                                    String imgURL = jsonUserDataObject.getString("picture");


                                                    // Print the user's data
                                                    Log.i(TAG, "Name: " + name);
                                                    Log.i(TAG, "Departure time: " + startTime);
                                                    Log.i(TAG, "Rating: " + rating);
                                                    Log.i(TAG, "Image URL: " + imgURL);

                                                    // Put the retrieved data into the current recommended drivers details entry in the array
                                                    recommendedDrivers[index] = new Utilities.UserDetails();
                                                    recommendedDrivers[index].setName(name);
                                                    recommendedDrivers[index].setRating(Double.parseDouble(rating));
                                                    recommendedDrivers[index].setImageURL(imgURL);
                                                    recommendedDrivers[index].setDepartureTime(departureTime);
                                                    recommendedDrivers[index].setSID(driverSID);
                                                    recommendedDrivers[index].setVID(drivervID);
                                                    recommendedDrivers[index].setAID(aID);

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                            @Override
                                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                                Log.i(TAG, "Failed to set trip:");
                                                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                                                error.printStackTrace();
                                            }
                                        });

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    Log.i(TAG, "Failed to set trip:");
                                    if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                                    error.printStackTrace();
                                }
                            });

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to request trip:");
                if (responseBody == null) {
                    Log.i(TAG, "Response body is null");
                }
//                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }

        });
        return recommendedDrivers;
    }

    /**
     * Used to select the aid as the lift the user wants to join.
     * Called from driver info activity.
     * @param context The context of the calling activity.
     * @param aid The active ID of the trip the user wants to join.
     */
    public void selectTrip(Context context, String aid) {
        JSONObject j = new JSONObject();
        try {
            j.put("aid", aid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = null;
        try {
            se = new StringEntity(j.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        AsyncHttpClient async = new AsyncHttpClient();
        async.addHeader("x-access-token", token);
        async.post(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/lift/selected" , se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Trip selection successful!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Trip selection failed!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
    }

    /**
     * This method is used to register a vehicle to the signed in user.
     * @param context The context of the calling activity
     * @param model The model of the vehicle
     * @param make The make of the vehicle
     * @param colour The colour of the vehicle
     * @param registrationNumber The registration number of the vehicle
     * @param numberOfSeats The number of seats available in the vehicle
     */
    public boolean registerVehicle(Context context, String model, String make, String colour, String registrationNumber, int numberOfSeats, final Bitmap vehicleImage) {

        JSONObject j = new JSONObject();
        try {
            j.put("model", model);
            j.put("make", make);
            j.put("color", colour);
            j.put("regNumber", registrationNumber);
            j.put("seats", numberOfSeats);
            j.put("sid", sID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = null;
        try {
            se = new StringEntity(j.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new SyncHttpClient();
        async.addHeader("x-access-token", token);
        Log.i(TAG, "TOKEN: " + token);
        Log.i(TAG, "SID: " + sID);
        c1 = context;
        async.post(context, "http://" + IP + ":" + portNumber + "/api/v1/vehicle", se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Vehicle registration completed successfully!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                registerVehicleSuccess = true; // Mark vehicle registration as successful

                // Now set the vehicles image using vehicleImage and the vID obtained from response body
                try {
                    JSONObject json = new JSONObject(new String(responseBody));
                    JSONObject jsonObject = json.getJSONObject("data");
                    final String vehicleID = jsonObject.getString("vid");

                    // Create a bitmap from the image url to give to setProfileImage rest call
                    class SetVehicleImageAsyncTask extends AsyncTask<Void, Void, Bitmap> {

                        @Override
                        protected void onPreExecute() {
                            // Do nothing
                        }

                        @Override
                        protected Bitmap doInBackground(Void... params) {

                            // Make the rest call to set the vehicle's image to the compressed image
                            setVehicleImage(c1, vehicleImage, vehicleID);
                            return vehicleImage;
                        }

                        @Override
                        protected void onPostExecute(Bitmap vehicleImage) {
                            // Make the rest call to set the user's profile image

                        }
                    }

                    new SetVehicleImageAsyncTask().execute();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Do nothing
            }
        });
        return registerVehicleSuccess;
    }

    /**
     * Updates the driver's driving status to 'status'
     * @param context The context of the calling activity
     * @param status True or False if the user wishes to be a driver or not respectively
     */
    public void updateDriverStatus(Context context, boolean status) {

        JSONObject j = new JSONObject();
        try {
            j.put("isDriver", status);
            j.put("sid", sID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = null;
        try {
            se = new StringEntity(j.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new SyncHttpClient();
        async.addHeader("x-access-token", token);
        Log.i(TAG, "TOKEN: " + token);
        Log.i(TAG, "SID: " + sID);
        async.put(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/driver" , se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Updated user's driver status successfully!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Update user's driver status failed!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
    }

    /**
     * tripDecided is called when a driver accepts a real-time ride request (from popup notification)
     * @param context the context of the calling activity
     * @param decision the decision the driver made, either to accept or decline the trip request.
     */
    public void tripDecided (Context context, boolean decision, String passengerID) {

        JSONObject j = new JSONObject();
        try {
            j.put("decided", decision);
            j.put("passenger", passengerID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = null;
        try {
            se = new StringEntity(j.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new AsyncHttpClient();
        async.addHeader("x-access-token", token);
        async.post(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/trip/" + aID + "/decided", se, "application/json", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "tripDecided successful");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "tripDecided failure!");
                error.printStackTrace();
            }
        });
    }

    /**
     * tripComplete is called on completion of a driver's trip which is when the driver's GPS coordinates
     * are within a certain range from their destination
     * @param context the context of the activity
     */
    public void tripComplete(Context context) {

        // No need to create a json object, so create string entity with empty string
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new SyncHttpClient();
        async.addHeader("x-access-token", token);
        async.post(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/trip/complete", se, "application/json", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "tripComplete successful");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "tripComplete failure!");
            }
        });
    }

    /**
     * passengerLiftComplete is called on completion of a passenger's trip which is when the passenger's GPS coordinates
     * are within a certain range from their destination
     * @param context the context of the activity
     */
    public void passengerLiftComplete(Context context) {

        // No need to create a json object, so create string entity with empty string
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new SyncHttpClient();
        async.addHeader("x-access-token", token);
        async.post(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/lift/complete", se, "application/json", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "passenger's liftComplete successful");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "passenger's liftComplete failure!");
                error.printStackTrace();
            }
        });
    }


    /**
     * Note this can be used in future work
     * @param context The context of the calling activity
     */
    public void retrieveVID(Context context) {

        // No JSON object contains nothing so create StringEntity from the empty String
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new AsyncHttpClient();
        async.addHeader("x-access-token", token);

        async.get(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/vehicles", se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Successfully retrieved VID!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));

                // Extract the VID from the from the response body
                try {
                    // Retrieve the token and store it in global variable 'token'
                    JSONObject json = new JSONObject(new String(responseBody));
                    JSONArray jsonVehicleArray = json.getJSONArray("data");

                    JSONObject jsonVehicleObject = jsonVehicleArray.getJSONObject(0);

                    vID = jsonVehicleObject.getString("vid");
                    Log.i(TAG, "VID: " + vID);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to set trip:");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
    }


    /**
     * Retrieve the users details, and return them in a comma delimited String
     * @param context Current context
     */
    public String retrieveUser(Context context, String sid) {
        driver = "";

        // No JSON object contains nothing so create StringEntity from the empty String
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient sync = new SyncHttpClient();
        sync.addHeader("x-access-token", token);

        sync.get(c1, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sid, se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Successfully retrieved users details!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));

                // Extract the user's name, departure time and rating
                try {
                    JSONObject json = new JSONObject(new String(responseBody));
                    JSONObject jsonUserDataObject = json.getJSONObject("data");

                    // User's name, rating and profile picture url
                    String name = jsonUserDataObject.getString("fullname");
                    String rating = jsonUserDataObject.getString("rating");
                    String imgURL = jsonUserDataObject.getString("picture");

                    // Print the driver's data
                    Log.i(TAG, "Name: " + name);
                    Log.i(TAG, "Rating: " + rating);
                    Log.i(TAG, "Image URL: " + imgURL);

                    driver = name + "," + rating + ",http://cdn3-www.dogtime.com/assets/uploads/gallery/puggle-dog-breed-picture-gallery/face-3.jpg" + imgURL;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to retrieve user's details:");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
        return driver;
    }

    /**
     * Retrieve the users details, and return them in a comma delimited String
     * @param context The current context of the calling activity
     * @param vid The ID of the vehicle we want to retrieve
     * @return a comma delimited string of the car's make, model and image url
     */
    public String retrieveVehicle(Context context, String vid) {
        car = "";

        // No JSON object contains nothing so create StringEntity from the empty String
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient sync = new SyncHttpClient();
        sync.addHeader("x-access-token", token);

        sync.get(context, "http://" + IP + ":" + portNumber + "/api/v1/vehicle/" + vid, se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Successfully retrieved vehicle's details!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));

                // Extract the user's name, departure time and rating
                try {
                    JSONObject json = new JSONObject(new String(responseBody));
                    JSONObject jsonVehicleDataObject = json.getJSONObject("data");

                    // User's name, rating and profile picture url
                    String make = jsonVehicleDataObject.getString("make");
                    String model = jsonVehicleDataObject.getString("model");
                    String imgURL = jsonVehicleDataObject.getString("picture");
                    String regNum = jsonVehicleDataObject.getString("regNumber");

                    // Print the driver's data
                    Log.i(TAG, "Make: " + make);
                    Log.i(TAG, "Model: " + model);
                    Log.i(TAG, "Image URL: " + imgURL);
                    Log.i(TAG, "Registration Num: " + regNum);

                    car = make + "," + model + "," + regNum;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to retrieve vehicle's details:");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
        return car;
    }

    /**
     * @param context The context of the calling activity
     * @param bitmap the bitmap that the user's image should be set to
     */
    public void setProfileImage(Context context, Bitmap bitmap) {
        // Compress the image
        ByteArrayOutputStream compressedProfileImage = new ByteArrayOutputStream();
        // Note, only jpeg images will work here, might need to find a general compression method
        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, compressedProfileImage);
        Bitmap decodedImg = BitmapFactory.decodeStream(new ByteArrayInputStream(compressedProfileImage.toByteArray()));

        RequestParams request = new RequestParams();
        if (decodedImg != null) {
            byte[] imageByteArr;
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            decodedImg.compress(Bitmap.CompressFormat.PNG, 0, bao);
            imageByteArr = bao.toByteArray();
            request.put("file", new ByteArrayInputStream(imageByteArr));
        }

        AsyncHttpClient async = new AsyncHttpClient();
        async.addHeader("x-access-token", token);

        async.post(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/image", request, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Successfully set user's profile image!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to set user's profile image:");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
    }

    /**
     * Gets the given user's profile picture as a Bitmap
     * @param context The context of the calling activity
     * @param stuberID The Stuber ID of the user whom we wish to get the profile picture of.
     * @return A bit map of the requested user's profile picture.
     */
    public Bitmap getProfileImage(Context context, String stuberID) {
        profileImg = null;

        // No JSON object contains nothing so create StringEntity from the empty String
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient sync = new SyncHttpClient();
        sync.addHeader("x-access-token", token);

        sync.get(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + stuberID + "/image", se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Successfully got the user's profile image!");
                // Subsample image from server to redeuce size needed to store in memory.
                //BitmapFactory.Options options = new BitmapFactory.Options();
                //options.inSampleSize = 8;
                profileImg = BitmapFactory.decodeByteArray(responseBody, 0, responseBody.length/*, options*/);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to get the user's profile image:");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
        return profileImg;
    }

    /**
     * @param context The context of the calling activity
     * @param bitmap the bitmap that the vehicle's image should be set to
     */
    public void setVehicleImage(Context context, Bitmap bitmap, String vehicleID) {
        // Compress the image
        ByteArrayOutputStream compressedVehicleImage = new ByteArrayOutputStream();
        // Note, only jpeg images will work here, might need to find a general compression method
        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, compressedVehicleImage);
        Bitmap decodedImg = BitmapFactory.decodeStream(new ByteArrayInputStream(compressedVehicleImage.toByteArray()));

        RequestParams request = new RequestParams();
        if (decodedImg != null) {
            byte[] imageByteArr;
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            decodedImg.compress(Bitmap.CompressFormat.PNG, 0, bao);
            imageByteArr = bao.toByteArray();
            request.put("file", new ByteArrayInputStream(imageByteArr));
        }

        AsyncHttpClient async = new SyncHttpClient();
        async.addHeader("x-access-token", token);

        async.setTimeout(30000); // 30 seconds till timeout

        async.post(context, "http://" + IP + ":" + portNumber + "/api/v1/vehicle/" + vehicleID + "/image", request, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Successfully set the vehicle's image!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Do nothing
            }
        });
    }

    /**
     * Gets the given vehicle ID's vehicle picture as a Bitmap
     * @param context The context of the calling activity
     * @param vehicleID The vehicle ID of the vehicle which we wish to get the picture of.
     * @return A bit map of the requested vehicles picture.
     */
    public Bitmap getVehicleImage(Context context, String vehicleID) {
        vehicleImg = null;

        // No JSON object contains nothing so create StringEntity from the empty String
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient sync = new SyncHttpClient();
        sync.addHeader("x-access-token", token);

        sync.get(context, "http://" + IP + ":" + portNumber + "/api/v1/vehicle/" + vehicleID + "/image", se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Successfully got the vehicle's image!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                vehicleImg = BitmapFactory.decodeByteArray(responseBody, 0, responseBody.length);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to get the vehicle's  image:");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
        return vehicleImg;
    }

    /**
     * Gets the user's inbox (ride requests made to the user in the last 90 seconds).
     * @param context The context of the calling activity.
     * @return A String containing the inbox details.
     */
    public String[] getInbox(Context context) throws java.net.SocketTimeoutException {
        String[] inboxArray = new String[2]; // First entry is message type, second is the message text
        // JSON object contains nothing so create StringEntity from the empty String
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient sync = new SyncHttpClient();
        sync.addHeader("x-access-token", token);

        sync.get(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/inbox", se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Successfully got the user's inbox!");
                //if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));

                // If the response received is empty, set the inbox message to contain ["empty"].
                if (responseBody == null) {
                    messageType = "No message type";
                    inboxMessage = "empty";
                } else {
                    try {
                        JSONObject json = new JSONObject(new String(responseBody));
                        JSONArray jsonDataArray = json.getJSONArray("data");
                        JSONObject jsonDataObject = jsonDataArray.getJSONObject(0);

                        // Extract the message type and message
                        messageType = jsonDataObject.getString("type");
                        inboxMessage = jsonDataObject.getString("text");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to get the user's inbox:");
                if (responseBody != null) if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
        inboxArray[0] = messageType;
        inboxArray[1] = inboxMessage;
        return inboxArray;
    }

    /**
     * Gets the specified user's profile information
     * @param context The calling activities context
     * @param userSID The SID of the user you want to get the details of
     * @return A Utilities.UserDetails object that contains the necessary user's details.
     */
    public Utilities.UserDetails getUser(Context context, String userSID) {
        if (userSID == null) userSID = sID; // If we did not set which user we want the result of, get the current user's details.
        final Utilities.UserDetails userDetails = new Utilities.UserDetails();
        // JSON object contains nothing so create StringEntity from the empty String
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient sync = new SyncHttpClient();
        sync.addHeader("x-access-token", token);

        sync.get(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + userSID, se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Successfully got the user's details!");

                try {
                    JSONObject json = new JSONObject(new String(responseBody));
                    JSONObject jsonUserDataObject = json.getJSONObject("data");

                    // User's name, pickup location and destination
                    String fullname = jsonUserDataObject.getString("fullname");
                    JSONArray tripHistoryJsonArray = jsonUserDataObject.getJSONArray("tripHistory");
                    // Get the latest trip in the user's trip history
                    JSONObject tripJSONObject = tripHistoryJsonArray.getJSONObject(0);
                    JSONObject destinationJSONObject = tripJSONObject.getJSONObject("destination");
                    JSONObject originJSONObject = tripJSONObject.getJSONObject("origin");

                    // Get the origin and destination coordinates
                    double originLat = originJSONObject.getDouble("lat");
                    double originLon = originJSONObject.getDouble("lon");
                    double destinationLat = destinationJSONObject.getDouble("lat");
                    double destinationLon = destinationJSONObject.getDouble("lon");

                    // Populate the userDetails object with the data retrieved above
                    userDetails.setName(fullname);
                    userDetails.setOriginLat(originLat);
                    userDetails.setOriginLon(originLon);
                    userDetails.setDestinationLat(destinationLat);
                    userDetails.setDestinationLon(destinationLon);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to get the user's details:");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
        return userDetails;
    }

    /**
     * Gets the specified user's trip requests
     * @param context The calling activities context
     * @return A Utilities.UserDetails object that contains the necessary user's details relating to the calling user's trip request.
     */
    public Utilities.UserDetails getTripRequest(Context context) {
        final Utilities.UserDetails userDetails = new Utilities.UserDetails();

        // Reinitialise the aID in case the user restarted the application.
        Utilities.UserDetails userDetails2 = getActiveDetails(c1);
        aID = userDetails2.getAID();
        // JSON object contains nothing so create StringEntity from the empty String
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient sync = new SyncHttpClient();
        sync.addHeader("x-access-token", token);


        sync.get(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/trip/requests/" + aID, se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Successfully got the user's trip requests!");

                try {
                    JSONObject json = new JSONObject(new String(responseBody));
                    JSONObject jsonDataObject = json.getJSONObject("data");

                    // User's name, pickup location and destination
                    JSONObject jsonPassengerObject = jsonDataObject.getJSONObject("passenger");
                    String fullname = jsonPassengerObject.getString("fullname");

                    JSONObject originJSONObject = jsonPassengerObject.getJSONObject("origin");
                    JSONObject destinationJSONObject = jsonPassengerObject.getJSONObject("destination");

                    // Get the origin and destination coordinates
                    double originLat = originJSONObject.getDouble("lat");
                    double originLon = originJSONObject.getDouble("lon");
                    double destinationLat = destinationJSONObject.getDouble("lat");
                    double destinationLon = destinationJSONObject.getDouble("lon");

                    // Populate the userDetails object with the data retrieved above
                    userDetails.setName(fullname);
                    userDetails.setOriginLat(originLat);
                    userDetails.setOriginLon(originLon);
                    userDetails.setDestinationLat(destinationLat);
                    userDetails.setDestinationLon(destinationLon);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to get the user's trip requests:");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
        return userDetails;
    }

    /**
     * Called when the passenger gets picked up by the driver
     * @param context The context of the calling activity
     */
    public void setLiftPickup(Context context) {

        // No need to create a json object, so create string entity with empty string
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new SyncHttpClient();
        async.addHeader("x-access-token", token);
        async.post(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/lift/pickup", se, "application/json", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Lift pickup successful");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Lift pickup failure!");
                error.printStackTrace();
            }
        });
    }

    /**
     * Lets the server know where the driver currently is, so passengers can see where their driver is.
     * @param context The context of the calling activity
     * @param lat The latitude of the driver
     * @param lon The longitude of the driver
     */
    public void updateDriversGPSCoords(Context context, double lat, double lon) {
        latitude = lat;
        longitude = lon;

        // JSON object contains nothing so create StringEntity from the empty String
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new SyncHttpClient();
        async.addHeader("x-access-token", token);

        c2 = context;

        async.get(c2, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID, se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Successfully retrieved the user's details for setting gps coordinates!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));

                // Extract the VID from the from the response body
                try {
                    // Retrieve the token and store it in global variable 'token'
                    JSONObject json = new JSONObject(new String(responseBody));
                    JSONObject jsonDataObject = json.getJSONObject("data");
                    boolean isDriver = jsonDataObject.getBoolean("isDriver");

                    // If the user is a driver, set their gps coordinates
                    if (isDriver) {
                        JSONObject j = new JSONObject();
                        try {
                            j.put("id", sID);
                            j.put("lat", latitude);
                            j.put("lon", longitude);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        StringEntity se = null;
                        try {
                            se = new StringEntity(j.toString());
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                        AsyncHttpClient async = new SyncHttpClient();
                        async.addHeader("x-access-token", token);
                        async.put(c2, "http://" + IP + ":" + portNumber + "/api/v1/gps/" + sID, se, "application/json", new AsyncHttpResponseHandler() {

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                Log.i(TAG, "Drivers GPS location updated successfully");
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                Log.i(TAG, "Drivers GPS location update failed, this does not matter");
                            }
                        });
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to retrieve the user's details for setting gps coordinates:");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
    }

    /**
     * Obtains the passenger's driver's GPS coordinates to display their location on the map.
     * @param context The context of the calling activity
     * @return a double array of lat and lon values [lat,lon]
     */
    public double[] getDriversGPSCoords(Context context) {
        final double[] coords = new double[2];
        // No need to create a json object, so create string entity with empty string
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new SyncHttpClient();
        async.addHeader("x-access-token", token);
        async.get(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/lift", se, "application/json", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Retrieved passengers ride details success");
                if (responseBody != null) {
                    try {
                        JSONObject json = new JSONObject(new String(responseBody));
                        JSONObject jsonDataObject = json.getJSONObject("data");
                        JSONObject jsonTripObject = jsonDataObject.getJSONObject("trip");
                        String driverSID = jsonTripObject.getString("sid");

                        // Now that we have the driver's SID, we can retrieve his GPS coordinates.
                        StringEntity se = null;
                        try {
                            se = new StringEntity("");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                        AsyncHttpClient sync = new SyncHttpClient();
                        sync.addHeader("x-access-token", token);

                        sync.get(c1, "http://" + IP + ":" + portNumber + "/api/v1/gps/" + driverSID, se, "application/json", new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                Log.i(TAG, "Retrieve driver's GPS coordinates successful!");
                                try {
                                    JSONObject json = new JSONObject(new String(responseBody));
                                    JSONObject jsonDataObject = json.getJSONObject("data");
                                    double driverLat = jsonDataObject.getDouble("lat");
                                    double driverLon = jsonDataObject.getDouble("lon");

                                    // Save the driver's coordinates in the coordinates array to be returned.
                                    coords[0] = driverLat;
                                    coords[1] = driverLon;

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                Log.i(TAG, "Retrieve driver's GPS coordinates failure");
                                error.printStackTrace();
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Retrieved passengers ride details success failure!");
                error.printStackTrace();
            }
        });
        return coords;
    }

    /**
     * This is called in a thread in MapActivity to check if the user is a driver or passenger and where their
     * destination is.
     * @param context
     */
    public Utilities.UserDetails getActiveDetails(Context context) {

        final Utilities.UserDetails userDetails = new Utilities.UserDetails();

        // No need to create a json object, so create string entity with empty string
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // Check if the user is the passenger of the current trip
        AsyncHttpClient sync = new SyncHttpClient();
        sync.addHeader("x-access-token", token);
        sync.get(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/lift", se, "application/json", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (responseBody != null) { // Continue if we got a result
                    Log.i(TAG, "passenger's lift information retrieval successful");
                    // This user is a passenger
                    userDetails.setIsDriver(false);
                    userDetails.setIsPassenger(true);

                    // Now get the passenger's destination
                    try {
                        JSONObject json = new JSONObject(new String(responseBody));
                        JSONObject jsonDataObject = json.getJSONObject("data");
                        JSONObject jsonTripObject = jsonDataObject.getJSONObject("trip");
                        JSONObject jsonDestinationObject = jsonTripObject.getJSONObject("destination");
                        double destinationLat = jsonDestinationObject.getDouble("lat");
                        double destinationLon = jsonDestinationObject.getDouble("lon");

                        // Now store the destination lat and lon in the user object we creating
                        userDetails.setDestinationLat(destinationLat);
                        userDetails.setDestinationLon(destinationLon);
                        // Update the driverSID in case the user restarted the application
                        driverSID = jsonTripObject.getString("sid");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Fail silently, we now know that this user is not currently a passenger.
            }
        });


        // Check if the user is the driver of the current trip
        AsyncHttpClient sync2 = new SyncHttpClient();
        sync2.addHeader("x-access-token", token);
        sync2.get(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/trip", se, "application/json", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (responseBody != null) { // Continue if we got a result
                    Log.i(TAG, "driver's lift information retrieval successful");
                    // This user is a driver
                    userDetails.setIsPassenger(false);
                    userDetails.setIsDriver(true);

                    // Now get the driver's destination
                    try {
                        JSONObject json = new JSONObject(new String(responseBody));
                        JSONObject jsonDataObject = json.getJSONObject("data");
                        JSONObject jsonTripObject = jsonDataObject.getJSONObject("trip");
                        JSONObject jsonDestinationObject = jsonTripObject.getJSONObject("destination");
                        double destinationLat = jsonDestinationObject.getDouble("lat");
                        double destinationLon = jsonDestinationObject.getDouble("lon");

                        // Now store the destination lat and lon in the user object we creating
                        userDetails.setDestinationLat(destinationLat);
                        userDetails.setDestinationLon(destinationLon);
                        userDetails.setAID(jsonTripObject.getString("aid"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Fail silently, we now know that this user is not currently a driver.
            }
        });
        return userDetails;
    }


    /**
     * Called when the passenger gets picked up by the driver
     * @param context The context of the calling activity
     */
    public void rateDriver(Context context, int rating) {

        JSONObject j = new JSONObject();
        try {
            j.put("reviewer", sID);
            j.put("reviewed", driverSID); // driverSID is null cos we restarted application
            j.put("message","");
            j.put("rating", rating);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = null;
        try {
            se = new StringEntity(j.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient async = new AsyncHttpClient();
        async.addHeader("x-access-token", token);
        async.post(context, "http://" + IP + ":" + portNumber + "/api/v1/review", se, "application/json", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Driver review successful");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Driver review failure!");
                error.printStackTrace();
            }
        });
    }

    /**
     * This is called when the user views their profile
     * Retrieve the users registered vehicle details, and return them in a comma delimited String
     * @param context The current context of the calling activity
     * @return a comma delimited string of the user's car's make, model and image url
     */
    public String retrieveUsersVehicle(Context context) {
        car = "";

        // No JSON object contains nothing so create StringEntity from the empty String
        StringEntity se = null;
        try {
            se = new StringEntity("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient sync = new SyncHttpClient();
        sync.addHeader("x-access-token", token);

        sync.get(context, "http://" + IP + ":" + portNumber + "/api/v1/user/" + sID + "/vehicles", se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "Successfully retrieved vehicle's details!");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));

                // Extract the user's name, departure time and rating
                try {
                    if (responseBody != null) {
                        JSONObject json = new JSONObject(new String(responseBody));
                        JSONArray jsonVehicleDataArray = json.getJSONArray("data");

                        // Retrieve the last registered vehicle
                        JSONObject jsonVehicleDataObject = jsonVehicleDataArray.getJSONObject(jsonVehicleDataArray.length() - 1);

                        // User's name, rating and profile picture url
                        String make = jsonVehicleDataObject.getString("make");
                        String model = jsonVehicleDataObject.getString("model");
                        String vid = jsonVehicleDataObject.getString("vid");
                        String colour = jsonVehicleDataObject.getString("color");
                        String registrationNumPlate = jsonVehicleDataObject.getString("regNumber");
                        String maxNumberOfPassengers = jsonVehicleDataObject.getInt("seats") - 1 + "";

                        // Print the driver's data
                        Log.i(TAG, "Make: " + make);
                        Log.i(TAG, "Model: " + model);
                        Log.i(TAG, "Colour: " + colour);
                        Log.i(TAG, "vID: " + vid);
                        Log.i(TAG, "Registration Num: " + registrationNumPlate);
                        Log.i(TAG, "Max Number of Seats: " + maxNumberOfPassengers);

                        car = make + "," + model + "," + colour + "," + maxNumberOfPassengers + "," + vid + "," + registrationNumPlate + "," + vid;
                    } else {
                        // If the user has no vehicles return an array such that I know no vehicles were found
                        car = "na,na,na,na,na,na,na";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Failed to retrieve vehicle's details:");
                if (responseBody != null) Log.i(TAG, "response body: " + new String(responseBody));
                error.printStackTrace();
            }
        });
        return car;
    }
}