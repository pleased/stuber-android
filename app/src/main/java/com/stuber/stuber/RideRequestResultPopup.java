/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class RideRequestResultPopup extends Activity implements View.OnClickListener {
    private TextView resultTxtView;
    private Button acceptButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_ride_request_result);

        // Initialise objects that are linked to items in the UI such as buttons, text, etc.
        resultTxtView = (TextView)findViewById(R.id.txt_view_driver_has_arrived);
        acceptButton = (Button)findViewById(R.id.btn_accept_request_result);

        // Set up action listeners for buttons
        acceptButton.setOnClickListener(this);

        // Obtain the device's resolution and create the popup size relative to that
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*0.8), (int)(height*0.6));

        // Show the passenger the result of their ride request
        resultTxtView.setText(getIntent().getStringExtra("requestResult"));
        acceptButton.setText(getIntent().getStringExtra("buttonText"));

        // Play a ping sound to notify the user
        MediaPlayer ping = MediaPlayer.create(this, R.raw.ping);
        ping.start();
    }

    @Override
    public void onClick(View v) {
        // Check which button got pressed
        switch (v.getId()) {
            case R.id.btn_accept_request_result:
                // Close the popup
                this.finish();
                break;
        }
    }
}
