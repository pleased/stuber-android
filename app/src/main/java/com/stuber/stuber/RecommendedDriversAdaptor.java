/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;

class RecommendedDriversAdaptor extends ArrayAdapter<String> {

    public RecommendedDriversAdaptor(Context context, String[] drivers) {
        super(context, R.layout.recommended_drivers_row, drivers);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater recommendedDriversInflator = LayoutInflater.from(getContext());
        View recommendedDriversView = recommendedDriversInflator.inflate(R.layout.recommended_drivers_row, parent, false);

        // Split the driver string above into its separate details and set the text of the
        // relevant text views below
        String[] driver = getItem(position).split(",");
        String name = driver[0];
        String departureTime = driver[1];
        String rating = driver[2];
        String imgURL = driver[3];

        TextView recommendedDriverName = (TextView) recommendedDriversView.findViewById(R.id.textViewRecommendedDriverName);
        TextView recommendedDriverDepartureTime = (TextView) recommendedDriversView.findViewById(R.id.textViewDepartureTime);
        TextView recommendedDriverRating = (TextView) recommendedDriversView.findViewById(R.id.textViewRating);
        new Utilities.DownloadImageTask((ImageView) recommendedDriversView.findViewById(R.id.recommnededDriverProfilePic)).execute(imgURL);

        // Set the text of the drivers names and set the profile pic
        recommendedDriverName.setText(name);
        recommendedDriverDepartureTime.setText(departureTime);
        recommendedDriverRating.setText(rating);

        return recommendedDriversView;
    }
}
