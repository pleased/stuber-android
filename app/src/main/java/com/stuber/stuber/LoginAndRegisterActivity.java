/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

public class LoginAndRegisterActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private SignInButton signInButton;
    private SignInButton registerButton;
    private TextView notificationTextView;

    private GoogleApiClient googleApiClient;
    private static final int SIGN_IN_REQ_CODE = 9001;
    private static final int REGISTER_IN_REQ_CODE = 9002;

    // Image below is hardcoded so that its the default profile image if the user does not have one
    String name, email, img_url = "http://i1.kym-cdn.com/photos/images/facebook/000/862/065/0e9.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_and_register);

        // Initialise objects that are linked to items in the UI such as buttons, text, etc.
        signInButton = (SignInButton)findViewById(R.id.btn_login);
        registerButton = (SignInButton)findViewById(R.id.btn_register);
        notificationTextView = (TextView)findViewById(R.id.txt_view_notification);

        // Setting Google request objects up
        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();

        // Setting up the action listener for the sign in and register buttons
        signInButton.setOnClickListener(this);
        registerButton.setOnClickListener(this);

        // Change the text of the register with Google button
        TextView textView = (TextView) registerButton.getChildAt(0);
        textView.setText(R.string.register_button);

        // Keep the screen from dimming
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Set notification text to contain nothing
        notificationTextView.setText("");
    }

    @Override
    public void onClick(View v) {
        // Check which button got pressed
        switch (v.getId()) {
            case R.id.btn_login:
                signOut();
                signIn();
                break;

            case R.id.btn_register:
                signOut();
                register();
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // Do nothing
    }

    /**
     * Opens the Google sign in activity
     */
    private void signIn() {
        if (isNetworkAvailable()) {
            Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
            startActivityForResult(intent, SIGN_IN_REQ_CODE);
        }
    }

    /**
     * Called before the user signs in or registers to ensure they have the option of selecting
     * which account they want to sign in or register with.
     */
    private void signOut() {
        if (isNetworkAvailable()) {
            Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    updateUI(false);
                }
            });
        }
    }

    /**
     * Opens the Google sign in activity
     */
    private void register() {
        if (isNetworkAvailable()) {
            Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
            startActivityForResult(intent, REGISTER_IN_REQ_CODE);
        }
    }

    /**
     * If the user can successfully log into their Google account then log into their Stuber account.
     * @param result The result obtained from the Google Sign-In
     */
    private void handleSignInResult(GoogleSignInResult result) {
        // Login was successful
        if (result.isSuccess()) {
            // Notify the user that they are being signed in
            notificationTextView.setTextColor(Color.WHITE);
            notificationTextView.setText(R.string.signing_in);
            // The signed in user's Google account containing all the information we need
            GoogleSignInAccount account = result.getSignInAccount();

            name = account.getDisplayName();
            email = account.getEmail();
            if (account.getPhotoUrl() != null) {
                img_url = account.getPhotoUrl().toString();
            }

            // Make a REST call to the backend to set the users default picture to their google img
            RestCalls r1 = new RestCalls();

            // Create a strict mode policy below to force the app to wait for sign in result
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            boolean signinResult = r1.login(this, account.getId());

            if (signinResult == true) { // If sign in was successful
                updateUI(true);
            } else {
                notificationTextView.setTextColor(Color.RED);
                notificationTextView.setText(R.string.sign_in_failed);
                Toast.makeText(getBaseContext(), R.string.sign_in_failed_toast, Toast.LENGTH_LONG).show();
            }

        } else {
            // If the login was unsuccessful or the user logged out hide the Prof_Section
            updateUI(false);
        }
    }

    /**
     * If the user successfully logs into their Google account, obtain the users name, email and
     * profile picture to register their Stuber account.
     * @param result The result obtained from the Google Sign-In
     */
    private void handleRegisterResult(GoogleSignInResult result) {
        // Login was successful
        if (result.isSuccess()) {
            // Notify the user that they are being registered
            notificationTextView.setTextColor(Color.WHITE);
            notificationTextView.setText(R.string.registering);
            // The signed in user's Google account containing all the information we need
            GoogleSignInAccount account = result.getSignInAccount();

            // Extract all the data from the Google account that we need
            name = account.getDisplayName();
            email = account.getEmail();
            if (account.getPhotoUrl() != null) {
                img_url = account.getPhotoUrl().toString();
            }

            // The following is not used in the back-end so it's faked on the front-end
            String[] likes = {"Apples", "Bananas"};
            String[] dislikes = {"Pears"};
            int age = 50;

            // If the user has already registered an account but I emptied the database reset their status
            SharedPreferences statusSharedPref = getSharedPreferences(email, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = statusSharedPref.edit();
            editor.putString(getString(R.string.driver_status), "neither");
            editor.commit();

            // Create a strict mode policy below to force the app to wait for registration result
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            RestCalls r1 = new RestCalls();
            boolean registerSuccess = r1.registerUser(this, account.getId(), account.getDisplayName(), account.getEmail(), likes, dislikes, age, img_url);

            if (registerSuccess == true) { // Registered successfully
                updateUI(true);
            } else {
                notificationTextView.setTextColor(Color.RED);
                notificationTextView.setText(R.string.registration_failed);
                Toast.makeText(getBaseContext(), R.string.registration_failed_toast, Toast.LENGTH_LONG).show();
            }

        } else {
            // If the registration was unsuccessful
            updateUI(false);
        }
    }

    private void updateUI(boolean isLogin) {
        // If the user has successfully logged in
        if (isLogin) {
            // Save the account info to SharedPreferences accountData
            SharedPreferences accountSharedPreferences = getSharedPreferences("accountData", Context.MODE_PRIVATE);
            SharedPreferences.Editor sharedPreferencesEditor = accountSharedPreferences.edit();
            sharedPreferencesEditor.putString("name", name);
            sharedPreferencesEditor.putString("email", email);
            sharedPreferencesEditor.putString("img_url", img_url);
            sharedPreferencesEditor.apply();

            // Switch to the Map activity upon successful login
            Intent intent = new Intent(this, MapActivity.class);
            startActivity(intent);
            this.finish();
        } else {
            // If the user has failed the sign in or has logged out do nothing
        }
    }

    /**
     * Gets the result from the Google Sign-In (or registration) and calls the appropriate methods
     * to handle the result.
     * @param requestCode The request code signifying either a sign in request or registration request
     * @param resultCode Shows if the sign in or registration was successful or not
     * @param data The data retrieved from the sign in intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGN_IN_REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else if (requestCode == REGISTER_IN_REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleRegisterResult(result);
        }
    }

    /**
     * Used to check if there is internet connection before trying to log in or register
     * @return true if there is connection to the internet, otherwise false
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(), R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                }
            });
            return false;
        }
    }
}
