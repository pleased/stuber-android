/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener, View.OnClickListener {

    // Global variables
    private GoogleMap mMap;
    private LocationManager locationManager;
    Location location;
    LatLng driverLocation, passengerLocation;
    private String mprovider;
    private Marker mUser, mDriver, mPassenger;
    Utilities.UserDetails passengersDetails;
    private String requestingPassengerSID;
    private Button menuButton;
    private Button goSomewhereButton;
    SharedPreferences statusSharedPref;
    // Timers used to schedule repeated tasks
    Timer inboxTimer = new Timer();
    Timer updateDriverCoordsTimer = new Timer();
    Timer getDriverCoordsTimer = new Timer();
    Timer checkPickupCompletedTimer = new Timer();
    Timer checkReachedDestinationTimer = new Timer();
    Timer checkDriverPickupCompletedTimer = new Timer();
    Drawer result; // The navigation drawer
    RestCalls r = new RestCalls();
    Bitmap personMarkerLarge, personMarker, vehicleMarkerLarge, vehicleMarker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Load the map marker resources
        personMarkerLarge = BitmapFactory.decodeResource(getResources(),R.drawable.hitchhiker_coloured_but_white);
        personMarker = Bitmap.createScaledBitmap(personMarkerLarge, 120, 174, false);
        vehicleMarkerLarge = BitmapFactory.decodeResource(getResources(),R.drawable.car_icon);
        vehicleMarker = Bitmap.createScaledBitmap(vehicleMarkerLarge, 96, 218, false);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Criteria criteria = new Criteria();
        // mprovider = locationManager.getBestProvider(criteria, false);  // Would use this normally, but wont work with app used to fake GPS location
        mprovider = locationManager.GPS_PROVIDER; // Changed to this for faking the GPS

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {}

        location = locationManager.getLastKnownLocation(mprovider);

        // Log out and notify the user if their GPS provider was not available, this can happen when
        // GPS is tuned off or faking GPS location screwed up the provider.
        if (location == null) {
            goToLoginAndRegisterActivity(getCurrentFocus());
            Toast.makeText(getBaseContext(), R.string.no_location_provider_found, Toast.LENGTH_SHORT).show();
            this.finish();
        } else {
            // Retrieve new GPS coordinates every 5 seconds for smoother animations
            locationManager.requestLocationUpdates(mprovider, 5000, 1, this);

            onLocationChanged(location);

            // Initialise objects that are linked to items in the UI such as buttons, text, etc.
            menuButton = (Button) findViewById(R.id.btn_menu_icon);
            goSomewhereButton = (Button) findViewById(R.id.btn_go_somewhere);

            // Change the UI elements to look better
            menuButton.getBackground().setAlpha(0);

            // Set up the action listeners for the buttons
            menuButton.setOnClickListener(this);
            goSomewhereButton.setOnClickListener(this);

            // Add items to navigation drawer
            PrimaryDrawerItem item_sign_up_to_drive = new PrimaryDrawerItem().withIdentifier(7).withName(R.string.drawer_item_sign_up_to_drive);
            PrimaryDrawerItem item_view_profile = new PrimaryDrawerItem().withIdentifier(8).withName(R.string.drawer_item_view_profile);
            PrimaryDrawerItem item_logout = new PrimaryDrawerItem().withIdentifier(10).withName(R.string.drawer_item_logout);

            // Obtain the account data from the accountData SharedPreferences
            SharedPreferences accountSharedPreferences = getSharedPreferences("accountData", Context.MODE_PRIVATE);
            String name = accountSharedPreferences.getString("name", getString(R.string.could_not_get_name));
            String email = accountSharedPreferences.getString("email", getString(R.string.could_not_get_email));
            String img_url = accountSharedPreferences.getString("img_url", getString(R.string.could_not_get_image));

            // Get a bitmap from the img_url (this is the user's profile image to be shown in the navigation drawer)
            // If receiving the image from the URL fails, the profile picture will be the cat placeholder.
            Bitmap profileImage = BitmapFactory.decodeResource(getResources(), R.drawable.cat_profile);
            try {
                URL url = new URL(img_url);
                profileImage = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (IOException e) {
            }

            // Build up the account header to be used in the drawer
            // Create the AccountHeader
            AccountHeader headerResult = new AccountHeaderBuilder()
                    .withActivity(this)
                    .withHeaderBackground(R.drawable.header) // Obtained from: http://androidessence.com/wp-content/themes/material-gaze/images/headers/blue.jpg
                    .addProfiles(
                            new ProfileDrawerItem().withName(name).withEmail(email).withIcon(profileImage)
                    )
                    .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                        @Override
                        public boolean onProfileChanged(View view, IProfile profile, boolean current) {
                            goToViewProfileActivity(view);
                            return true;
                        }
                    })
                    .build();
            // Create the drawer and remember the `Drawer` result object
            // The result object obtained can be used to alter the menu later, like add notification number etc.
            result = new DrawerBuilder()
                    .withActivity(this)
                    .withAccountHeader(headerResult)
                    .addDrawerItems(
                            item_sign_up_to_drive,
                            item_view_profile,
                            item_logout
                    )
                    .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                        @Override
                        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                            // do something with the clicked item :D
//                            result.closeDrawer();
                            if (drawerItem.equals(5)) {
                                goToPendingRideRequestsActivity(view);
                            } else if (drawerItem.equals(7)) {
                                goToRegisterVehicleActivity(view);
                            } else if (drawerItem.equals(8)) {
                                goToViewProfileActivity(view);
                            } else if (drawerItem.equals(10)) {
                                goToLoginAndRegisterActivity(view);
                            }
                            return true;
                        }
                    })
                    .build();



            // Stop the screen from dimming
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            // Obtain the user's driver/passenger status from shared preferences
            Context context = getBaseContext();

            // Create a shared preferences file with the name as the user's email address (so each user on same device has his own shared preferences file)
            // and use this shared preferences file to store the status of the user as 'driver', 'passenger' or 'neither'.
            statusSharedPref = context.getSharedPreferences(email, Context.MODE_PRIVATE);

            // Start a thread that polls for inbox messages every 5 seconds.
            //new PollForInboxMessagesAsyncTask().execute();
            inboxTimer.schedule(new PollServerForInboxMessages(), 10000, 10000);

            // Update the drivers location every 5 seconds
            updateDriverCoordsTimer.schedule(new updateDriversGPSCoords(), 10000, 10000);

            // Retrieve the drivers coordinates every 5 seconds to check if this user has been picked up.
            getDriverCoordsTimer.schedule(new getDriversGPSCoords(), 10000, 10000);

            // Check if the driver has picked up the passenger every 10 seconds (only if they are on a trip)
            String status = statusSharedPref.getString(getString(R.string.driver_status), "neither");
            if (status.equals("neither")) {
                checkPickupCompletedTimer.schedule(new checkPickupCompleted(), 10000, 10000);
            }

            // Check if the user has reached their destination every 10 seconds
            checkReachedDestinationTimer.schedule(new checkDropOffCompleted(), 10000, 10000);
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker at the user's current location.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
        if (mUser == null) {
            mUser = mMap.addMarker(new MarkerOptions().position(currentLocation).title("Current Location").flat(true));

            // Set the user's icon according to their driver/passenger status
            String status = statusSharedPref.getString(getString(R.string.driver_status), "neither");
            if (status.equals("neither")) {
                mUser.setIcon(BitmapDescriptorFactory.fromBitmap(personMarker));
            } else {
                mUser.setIcon(BitmapDescriptorFactory.fromBitmap(vehicleMarker));
            }
            mUser.setVisible(true);
        } else {
            animateMarker(mUser, currentLocation, false);
            mUser.setVisible(true);
        }

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                .zoom(17)                   // Sets the zoom
                .bearing(0)                 // Sets the orientation of the camera to east
                .tilt(40)                   // Sets the tilt of the camera to 40 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 1, null);
        // Remove the compass from top right, since it sits behind the menu icon
        mMap.getUiSettings().setCompassEnabled(false);
    }

    @Override
    public void onLocationChanged(Location location) {
        // Create new LatLng from updated location and move the camera and marker there
        LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
        if (mMap != null) {
            // Initialise the marker if not already done
            if (mUser == null) {
                mUser = mMap.addMarker(new MarkerOptions().position(currentLocation).title(getString(R.string.current_location)).flat(true));
                // Set the user's icon according to their driver/passenger status
                String status = statusSharedPref.getString(getString(R.string.driver_status), "neither");
                if (status.equals("neither")) {
                    mUser.setIcon(BitmapDescriptorFactory.fromBitmap(personMarker));
                } else {
                    mUser.setIcon(BitmapDescriptorFactory.fromBitmap(vehicleMarker));
                }
            } else {
                animateMarker(mUser, currentLocation, false);
            }

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(0)                 // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 40 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 5000, null);
        }
        // Update the global location
        this.location = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // Do nothing
    }

    @Override
    public void onProviderEnabled(String provider) {
        // Do nothing
    }

    @Override
    public void onProviderDisabled(String provider) {
        // Logout if GPS provider gets disabled. Can't use app with no GPS...
        goToLoginAndRegisterActivity(getCurrentFocus());
        Toast.makeText(getBaseContext(), "No Location Provider Found", Toast.LENGTH_SHORT).show();
        this.finish();

    }

    /** Called when the user taps the Go somewhere navigation drawer item */
    public void goToRideDetailsActivity(View view, Location loc) {
        Intent intent = new Intent(this, RideDetailsActivity.class);
        intent.putExtra("originLat", location.getLatitude()+"");
        intent.putExtra("originLon", location.getLongitude()+"");
        startActivity(intent);
    }

    /** Called when the user taps the sign up to drive navigation drawer item */
    public void goToRegisterVehicleActivity(View view) {
        Intent intent = new Intent(this, SignUpToDriveActivity.class);
        startActivity(intent);
    }

    /** Called when the user taps the view pending ride requests navigation drawer item */
    public void goToPendingRideRequestsActivity(View view) {
        Intent intent = new Intent(this, PendingRideRequestsActivity.class);
        startActivity(intent);
    }

    /** Called when the user taps the view profile navigation drawer item */
    public void goToViewProfileActivity(View view) {
        Intent intent = new Intent(this, ViewProfileActivity.class);
        startActivity(intent);
    }

    /** Called when the passenger's driver arrives */
    public void displayDriverHasArrivedPopup() {
        Intent intent = new Intent(this, DriverArrivedPopup.class);
        startActivity(intent);
    }

    /** Called when the passenger arrives at their destination for them to rate their driver */
    public void displayRateDriverPopup() {
        Intent intent = new Intent(this, RateDriverPopup.class);
        startActivity(intent);
    }

    /** Called when the user taps the view profile navigation drawer item */
    public void goToLoginAndRegisterActivity(View view) {
        // First stop polling the user's inbox then go to login and register activity
        inboxTimer.cancel();
        updateDriverCoordsTimer.cancel();
        getDriverCoordsTimer.cancel();
        checkPickupCompletedTimer.cancel();
        checkReachedDestinationTimer.cancel();
        inboxTimer.purge();
        updateDriverCoordsTimer.purge();
        getDriverCoordsTimer.purge();
        checkPickupCompletedTimer.purge();
        checkReachedDestinationTimer.purge();

        // Set the users map icon to null so it gets reinitialised when they log back in
        mUser = null;
        this.finish();

        Intent intent = new Intent(this, LoginAndRegisterActivity.class);
        startActivity(intent);
    }

    /** Called when the user receives a real-time ride request */
    public void displayRideRequestPopup(String passengerSID, String passengerName, String origin, String destination) {
        Intent intent = new Intent(this, RideRequestPopup.class);
        intent.putExtra("requestingPassengerSID", passengerSID);
        intent.putExtra("requestingPassengerName", passengerName);
        intent.putExtra("requestingPassengerOriginAddress", origin);
        intent.putExtra("requestingPassengerDestinationAddress", destination);
        startActivity(intent);
    }

    /**
     * Called when the user receives a real-time ride request result
     * @param requestResult The text telling the user if their request was accepted or declined
     * @param buttonText The text the button in the popup should display to the requesting passenger
     */
    public void displayRideRequestResultPopup(String requestResult, String buttonText) {
        Intent intent = new Intent(this, RideRequestResultPopup.class);
        intent.putExtra("requestResult", requestResult);
        intent.putExtra("buttonText", buttonText);
        startActivity(intent);
    }

    /**
     * This method is used to animate the movement of the marker upon users locations
     * changing. This is adapted from: https://stackoverflow.com/questions/13728041/move-markers-in-google-map-v2-android
     * https://developers.google.com/maps/documentation/android-api/map
     * @param marker The marker to be moved
     * @param toPosition The position to move the marker to (LatLng)
     * @param hideMarker Whether or not to hide the marker
     */
    public void animateMarker(final Marker marker, final LatLng toPosition, final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 5000; // Take 5 seconds to travel to new location (same as GPS update frequency)

        // The first time the marker is initialised the startLatLng defaults to 0,0 so don't animate
        // it moving from there to real location, just draw it at real location.
        if (startLatLng != null) {
            if (startLatLng.latitude == 0 && startLatLng.longitude == 0) {
                marker.setPosition(toPosition);
            } else {

                final Interpolator interpolator = new LinearInterpolator();

                if (startLatLng != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            long elapsed = SystemClock.uptimeMillis() - start;
                            float t = interpolator.getInterpolation((float) elapsed / duration);
                            double lng = t * toPosition.longitude + (1 - t) * startLatLng.longitude;
                            double lat = t * toPosition.latitude + (1 - t) * startLatLng.latitude;
                            marker.setPosition(new LatLng(lat, lng));

                            if (t < 1.0) {
                                // Post again 16ms later.
                                handler.postDelayed(this, 16);
                            } else {
                                if (hideMarker) {
                                    marker.setVisible(false);
                                } else {
                                    marker.setVisible(true);
                                }
                            }
                        }
                    });
                }
            }
        }
    }

    /**
     * The action listeners
     * @param v The current view
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_menu_icon:
                result.openDrawer();
                break;
            case R.id.btn_go_somewhere:
                goToRideDetailsActivity(v, location);
                break;
        }
    }

    /**
     * This timer task checks the user's inbox every 10 seconds (specified where the timer object is
     * created) and performs operations based on what it finds in the inbox.
     */
    private class PollServerForInboxMessages extends TimerTask {
        public void run() {
            if (isNetworkAvailable()) { // First check there is internet connection
                String[] messageArray = new String[0];
                try {
                    messageArray = r.getInbox(MapActivity.this);
                } catch (SocketTimeoutException e) {
                    e.printStackTrace();
                }
                String messageType = messageArray[0];
                String message = messageArray[1];
                String[] text = message.split(" ");

                switch (messageType) {
                    case "CONFIRM":
                        // Display the popup activity if there is a passenger requesting a real-time lift.
                        requestingPassengerSID = text[text.length - 1];
                        if (!requestingPassengerSID.equals("empty")) {
                            // Now obtain this requesting passengers details and display them on a popup.
                            // Details needed: Passenger name, Pickup address and Destination address.

                            // Get the requesting passengers name, origin and destination and pass it to displayRideRequestPopup
                            passengersDetails = r.getTripRequest(MapActivity.this);

                            // Convert the passengers origin and destination coordinates into address strings
                            Geocoder geocoder;
                            final List<Address> originAddress, destinationAddress;
                            geocoder = new Geocoder(MapActivity.this, Locale.getDefault());

                            String originAddressName = "originAddress", destinationAddressName = "destinationAddress";

                            try {
                                originAddress = geocoder.getFromLocation(passengersDetails.getOriginLat(), passengersDetails.getOriginLon(), 1);
                                destinationAddress = geocoder.getFromLocation(passengersDetails.getDestinationLat(), passengersDetails.getDestinationLon(), 1);
                                originAddressName = originAddress.get(0).getAddressLine(0);
                                destinationAddressName = destinationAddress.get(0).getAddressLine(0);

                                // Extract only the street address from all the address information obtained
                                originAddressName = originAddressName.split(",")[0];
                                destinationAddressName = destinationAddressName.split(",")[0];

                                // Now draw this passenger on the driver's map
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        passengerLocation = new LatLng(passengersDetails.getOriginLat(), passengersDetails.getOriginLon());
                                        if (mPassenger == null) {
                                            mPassenger = mMap.addMarker(new MarkerOptions().position(passengerLocation).title(getString(R.string.current_location)).flat(true));
                                            mPassenger.setIcon(BitmapDescriptorFactory.fromBitmap(personMarker));
                                            checkDriverPickupCompletedTimer.schedule(new checkDriverPickupCompleted(), 10000, 10000);
                                        } else {
                                            mPassenger.setVisible(true);
                                            animateMarker(mPassenger, passengerLocation, false);
                                        }
                                    }
                                });
                            } catch (IOException e) {
                                // Could not convert the coordinates to an address
                                e.printStackTrace();
                            }
                            displayRideRequestPopup(requestingPassengerSID, passengersDetails.getName(), originAddressName, destinationAddressName);
                        }
                        break;
                    case "CONFIRM_RESULT_TRUE":
                        displayRideRequestResultPopup(getString(R.string.your_ride_request_was_accepted), getString(R.string.awesome));
                        break;
                    case "CONFIRM_RESULT_FALSE":
                        displayRideRequestResultPopup(getString(R.string.your_ride_request_was_declined), getString(R.string.aww_ok));
                        break;
                }
            }
        }
    }

    /**
     * A thread that periodically checks if this passenger's driver is within a certain range
     * to determine if they have been picked up or not.
     */
    private class checkPickupCompleted extends TimerTask {
        public void run() {
            if (isNetworkAvailable()) { // First check there is internet connection
                // Check if the driver is within 50 meters of the passenger
                if (driverLocation != null) {
                    if (Utilities.isUserInRangeForPickup(driverLocation.latitude, driverLocation.longitude, location.getLatitude(), location.getLongitude())) {
                        r.setLiftPickup(MapActivity.this);
                        checkPickupCompletedTimer.cancel();
                        checkPickupCompletedTimer.purge();
                        displayDriverHasArrivedPopup();

                        // Hide the driver's icon and change the user's passenger icon into a vehicle since the lift has commenced
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (mDriver != null) mDriver.remove();
                                mUser.setIcon(BitmapDescriptorFactory.fromBitmap(vehicleMarker));
                                getDriverCoordsTimer.cancel();
                                getDriverCoordsTimer.purge();
                            }
                        });

                        // The user has been picked up to set their status in shared preferences to 'passenger'
                        SharedPreferences.Editor editor = statusSharedPref.edit();
                        editor.putString(getString(R.string.driver_status), "passenger");
                        editor.commit();
                    }
                }
            }
        }
    }

    /**
     * A thread that periodically checks if this passenger is within a certain range of their
     * destination to see if they have been dropped off or not.
     */
    private class checkDropOffCompleted extends TimerTask {
        public void run() {
            if (isNetworkAvailable()) { // First check there is internet connection
                // Check to see if the location service has changed
                // (This will happen when I use an app to fake my GPS coordinates for the sake of the demo)
                // Note: this is not needed if you do not fake the GPS coordinates.
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (mprovider != null && !mprovider.equals("")) {
                    if (ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    location = locationManager.getLastKnownLocation(mprovider);
                }

                Utilities.UserDetails userDetails = r.getActiveDetails(MapActivity.this);
                if (userDetails.getIsDriver()) {
                    SharedPreferences.Editor editor = statusSharedPref.edit();
                    editor.putString(getString(R.string.driver_status), "driver");
                    editor.commit();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mUser != null) {
                                mUser.setIcon(BitmapDescriptorFactory.fromBitmap(vehicleMarker));
                            }
                        }
                    });
                    if (Utilities.isUserInRangeForDropOff(userDetails.getDestinationLat(), userDetails.getDestinationLon(), location.getLatitude(), location.getLongitude())) {
                        r.tripComplete(MapActivity.this);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mUser.setIcon(BitmapDescriptorFactory.fromBitmap(personMarker));
                            }
                        });

                        // Set the user's status in shared preference to 'neither' driver or passenger
                        SharedPreferences.Editor editor2 = statusSharedPref.edit();
                        editor2.putString(getString(R.string.driver_status), "neither");
                        editor2.commit();

                        // Stop polling the server to see if the trip is completed
                        checkPickupCompletedTimer.cancel();
                        checkReachedDestinationTimer.purge();
                    }
                } else if (userDetails.getIsPassenger()) {
                    if (Utilities.isUserInRangeForDropOff(userDetails.getDestinationLat(), userDetails.getDestinationLon(), location.getLatitude(), location.getLongitude())) {
                        r.passengerLiftComplete(MapActivity.this);
                        checkReachedDestinationTimer.cancel();
                        checkReachedDestinationTimer.purge();

                        // The user is no longer a passenger so set their status to 'neither'
                        SharedPreferences.Editor editor3 = statusSharedPref.edit();
                        editor3.putString(getString(R.string.driver_status), "neither");
                        editor3.commit();

                        displayRateDriverPopup();

                        // Stop polling the server to see if the lift is completed
                        checkReachedDestinationTimer.cancel();
                        checkReachedDestinationTimer.purge();

                        // Restart the timer to see if driver has arrived
                        getDriverCoordsTimer = new Timer();
                        getDriverCoordsTimer.schedule(new getDriversGPSCoords(), 10000, 10000);

                        // Show the user's passenger marker again since the lift is completed
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mUser.setIcon(BitmapDescriptorFactory.fromBitmap(personMarker));
                            }
                        });
                    }
                }
            }
        }
    }

    /**
     * A timer task that periodically updates the driver's GPS coordinates
     */
    private class updateDriversGPSCoords extends TimerTask {
        public void run() {
            if (isNetworkAvailable()) { // First check there is internet connection
                r.updateDriversGPSCoords(MapActivity.this, location.getLatitude(), location.getLongitude());
            }
        }
    }

    /**
     * A timer task that periodically fetches the drivers GPS coordinates
     */
    private class getDriversGPSCoords extends TimerTask {
        public void run() {
            if (isNetworkAvailable()) { // First check there is internet connection
                double[] coords = r.getDriversGPSCoords(MapActivity.this);

                // Now draw the driver's car there
                driverLocation = new LatLng(coords[0], coords[1]);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // If the driver's car has not yet been drawn, then draw it, else move it to their new location
                        // The 'driver's location' by default is 0,0 if you don't have a driver, so in that case don't draw the driver.
                        if (mDriver == null && driverLocation.latitude != 0 && driverLocation.longitude != 0) {
                            mDriver = mMap.addMarker(new MarkerOptions().position(driverLocation).title("Driver").flat(true));
                            mDriver.setIcon(BitmapDescriptorFactory.fromBitmap(vehicleMarker));
                        } else if (driverLocation != null && driverLocation.latitude != 0 && driverLocation.longitude != 0) {
                            animateMarker(mDriver, driverLocation, false);
                        }
                    }
                });
            }
        }
    }

    /**
     * A thread that periodically checks if this driver is within a certain range
     * from their passenger to determine if the pickup has been completed or not.
     */
    private class checkDriverPickupCompleted extends TimerTask {
        public void run() {
            if (isNetworkAvailable()) { // First check there is internet connection
                // Check if the driver is within 50 meters of the passenger
                if (location != null && passengerLocation != null) {
                    if (Utilities.isUserInRangeForPickup(passengerLocation.latitude, passengerLocation.longitude, location.getLatitude(), location.getLongitude())) {
                        runOnUiThread(new Runnable() {
                                          @Override
                                          public void run() {
                                              mPassenger.setVisible(false);
                                          }
                        });
                        checkDriverPickupCompletedTimer.cancel();
                        checkDriverPickupCompletedTimer.purge();
                    }
                }
            }
        }
    }

    /**
     * A method called before each thread executes to ensure there is internet connection
     * @return True if there is indeed internet connection, False otherwise
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                }
            });
            goToLoginAndRegisterActivity(getCurrentFocus());
            return false;
        }
    }
}
