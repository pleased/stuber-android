/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Arrays;

public class ViewProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private Button doneButton;
    private TextView usernameTextView;
    private TextView makeAndModelLabelTextView;
    private TextView colourLabelTextView;
    private TextView maxNumPassengersLabelTextView;
    private TextView registeredVehicleNotificationTextView;
    private ImageView profilePictureImageView;
    String[] registeredVehicle;
    ListAdapter registeredVehicleAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        // Create references to UI elements
        doneButton = (Button)findViewById(R.id.buttonDone);
        usernameTextView = (TextView)findViewById(R.id.textViewProfileName);
        makeAndModelLabelTextView = (TextView)findViewById(R.id.textViewMakeAndModel);
        colourLabelTextView = (TextView)findViewById(R.id.textViewColour);
        maxNumPassengersLabelTextView = (TextView)findViewById(R.id.textViewMaxNumPassengers);
        registeredVehicleNotificationTextView = (TextView)findViewById(R.id.textViewRegisteredVehicleNotification);
        profilePictureImageView = (ImageView)findViewById(R.id.imageViewProfilePicture);

        // Set up the action listeners for the buttons
        doneButton.setOnClickListener(this);

        // Obtain the user's account data from shared preferences accountData
        SharedPreferences accountSharedPreferences = getSharedPreferences("accountData", Context.MODE_PRIVATE);
        usernameTextView.setText(accountSharedPreferences.getString("name", "Could not get name"));
        new Utilities.DownloadImageTask((ImageView) findViewById(R.id.imageViewProfilePicture))
                .execute(accountSharedPreferences.getString("img_url", "Could not obtain image"));

        // Stop the screen from dimming
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Create the adaptor used to show the user's registered vehicle
        registeredVehicle = new String[]{""};
        registeredVehicleAdaptor = new RegisteredVehicleAdaptor(this, registeredVehicle);

        new LoadRegisteredVehicleAsyncTask().execute();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonDone:
                // Go back to the map activity
                this.finish();
                break;
        }
    }

    /**
     * An async task used to load the registered vehicle's information
     */
    private class LoadRegisteredVehicleAsyncTask extends AsyncTask<Void, Void, String[]> {

        @Override
        protected void onPreExecute() {
            // start loading animation maybe?
            makeAndModelLabelTextView.setVisibility(View.INVISIBLE);
            colourLabelTextView.setVisibility(View.INVISIBLE);
            maxNumPassengersLabelTextView.setVisibility(View.INVISIBLE);
            registeredVehicleNotificationTextView.setText(R.string.loading_vehicle_information);
        }

        @Override
        protected String[] doInBackground(Void... params) {

            RestCalls r = new RestCalls();
            String vehicle = r.retrieveUsersVehicle(getBaseContext());

            // Build up an array of the potential drivers details, 1 entry = 1 driver with details
            // separated by commas
            if (!vehicle.equals("na,na,na,na,na,na,na")) {
                String[] vehicleArr = {vehicle};
                return vehicleArr;
            } else return null; // If there were no lift matches, return null and handle it in onPostExecute
        }

        @Override
        protected void onPostExecute(String[] vehicle) {
            // stop the loading animation or something

            if (vehicle != null) {
                // Display the headings
                makeAndModelLabelTextView.setVisibility(View.VISIBLE);
                colourLabelTextView.setVisibility(View.VISIBLE);
                maxNumPassengersLabelTextView.setVisibility(View.VISIBLE);
                registeredVehicleNotificationTextView.setVisibility(View.INVISIBLE);

                // An adapter that takes the information in the arrays above and puts them into list items.
                ListAdapter recommendedDriversAdaptor = new RegisteredVehicleAdaptor(ViewProfileActivity.this, vehicle);

                // Create references to the UI elements
                ListView recommendedDriversListView = (ListView) findViewById(R.id.list_view_vehicles);

                // Set the list view of recommended drivers to use the adaptor created above
                recommendedDriversListView.setAdapter(recommendedDriversAdaptor);

                // Setting up the action listener for the list items
                recommendedDriversListView.setOnItemClickListener(
                        new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                String[] selectedVehicleDetails = String.valueOf(parent.getItemAtPosition(position)).split(",");
                                String selectedVehicleMake = selectedVehicleDetails[selectedVehicleDetails.length - 7];
                                String selectedVehicleModel = selectedVehicleDetails[selectedVehicleDetails.length - 6];
                                String selectedVehicleColour = selectedVehicleDetails[selectedVehicleDetails.length - 5];
                                String selectedVehicleMaxNumSeats = selectedVehicleDetails[selectedVehicleDetails.length - 4];
                                String selectedVehicleVID = selectedVehicleDetails[selectedVehicleDetails.length - 3];
                                String selectedVehicleRegNum = selectedVehicleDetails[selectedVehicleDetails.length - 2];

                                // Go to the sign up to drive activity with the fields filled in already
                                goToSignUpToDriveActivity(selectedVehicleMake, selectedVehicleModel, selectedVehicleColour, selectedVehicleMaxNumSeats, selectedVehicleRegNum, selectedVehicleVID);
                            }
                        }

                );
            } else {
                registeredVehicleNotificationTextView.setText(R.string.you_have_no_registered_vehicles);
            }
        }
    }

    /**
     * Goes to the sign up to drive activity with the details already filled in
     * @param selectedVehicleMake The vehicles make
     * @param selectedVehicleModel The vehicles model
     * @param selectedVehicleColour The vehicles colour
     * @param selectedVehicleMaxNumSeats The vehicles max number of seats
     * @param selectedVehicleRegNum The vehicles registration number
     * @param selectedVehicleVID  The vehicles vID
     */
    public void goToSignUpToDriveActivity(String selectedVehicleMake, String selectedVehicleModel, String selectedVehicleColour, String selectedVehicleMaxNumSeats, String selectedVehicleRegNum, String selectedVehicleVID) {
        Intent intent = new Intent(this, SignUpToDriveActivity.class);
        intent.putExtra("make", selectedVehicleMake);
        intent.putExtra("model", selectedVehicleModel);
        intent.putExtra("colour", selectedVehicleColour);
        intent.putExtra("regNum", selectedVehicleRegNum);
        intent.putExtra("vid", selectedVehicleVID);
        intent.putExtra("maxNumPassengers", selectedVehicleMaxNumSeats);
        startActivity(intent);
        this.finish();
    }
}
