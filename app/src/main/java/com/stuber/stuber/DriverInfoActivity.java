/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.stuber.stuber;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DriverInfoActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView profilePictureImageView;
    private ImageView carPictureImageView;
    private TextView nameTextView;
    private TextView makeAndModelTextView;
    private TextView registrationNumberTextView;
    private TextView averageRatingTextView;
    private Button acceptDriverButton;

    private String selectedDriverAID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_info);

        // Create references to UI elements
        profilePictureImageView = (ImageView) findViewById(R.id.imageViewDriverProfilePicture);
        carPictureImageView = (ImageView) findViewById(R.id.imageViewDriversCar);
        nameTextView = (TextView) findViewById(R.id.textViewDriverName);
        makeAndModelTextView = (TextView) findViewById(R.id.textViewCarMakeAndModel);
        registrationNumberTextView = (TextView) findViewById(R.id.textViewRegistrationNumber);
        averageRatingTextView = (TextView) findViewById(R.id.textViewAverageRating);
        acceptDriverButton = (Button) findViewById(R.id.buttonAcceptDriver);

        // Create action listeners
        acceptDriverButton.setOnClickListener(this);

        // Stop the screen from dimming
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Execute the AsyncTasks defined below to populate the UI elements with the selected driver's info
        new LoadDriverInfoAsyncTask().execute();
        new LoadDriverImageAsyncTask().execute();
        new LoadVehicleImageAsyncTask().execute();
    }

    @Override
    public void onClick(View v) {
        // Check which button got pressed
        switch (v.getId()) {
            case R.id.buttonAcceptDriver:
                // Accept the driver
                RestCalls r = new RestCalls();
                r.selectTrip(this, selectedDriverAID);

                // Close the recommended drivers activity
                RecommendedDriversActivity.recommendedDriversActivity.finish();

                // Go back to the map activity
                Toast.makeText(DriverInfoActivity.this, "Driver requested!", Toast.LENGTH_LONG).show();
                this.finish();
                break;
        }
    }

    /**
     * An AsyncTask used to load the image of the selected driver's vehicle.
     */
    private class LoadVehicleImageAsyncTask extends AsyncTask<Void, Void, String> {
        Bitmap vehicleImg;

        @Override
        protected void onPreExecute() {
            // Future work: add loading animation here
        }

        @Override
        protected String doInBackground(Void... params) {
            // Obtain the intent extras that will be used to create the rest calls to get drivers and vehicles details
            String selectedDriverVID = getIntent().getStringExtra("vID");

            RestCalls r = new RestCalls();
            vehicleImg = r.getVehicleImage(DriverInfoActivity.this, selectedDriverVID);

            // Don't need anything to be returned, just the knowledge that the image has been initialised.
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            carPictureImageView.setImageBitmap(vehicleImg);
        }
    }

    /**
     * An AsyncTask used to load the driver's profile image
     */
    private class LoadDriverImageAsyncTask extends AsyncTask<Void, Void, String> {
        Bitmap profileImg;

        @Override
        protected void onPreExecute() {
            // Future work: add loading animation here
        }

        @Override
        protected String doInBackground(Void... params) {
            // Obtain the intent extras that will be used to create the rest calls to get drivers and vehicles details
            String selectedDriverSID = getIntent().getStringExtra("driverSID");

            RestCalls r = new RestCalls();
            profileImg = r.getProfileImage(DriverInfoActivity.this, selectedDriverSID);

            // Don't need anything to be returned, just the knowledge that the image has been initialised.
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            if (profileImg != null) {
                profilePictureImageView.setImageBitmap(profileImg);
            }
        }
    }

    /**
     * An AsyncTask used to load the drivers details such as: name, rating etc.
     */
    private class LoadDriverInfoAsyncTask extends AsyncTask<Void, Void, String[]> {

        @Override
        protected void onPreExecute() {
            // Future work: add loading animation here
        }

        @Override
        protected String[] doInBackground(Void... params) {
            // Obtain the intent extras that will be used to create the rest calls to get drivers and vehicles details
            String selectedDriverSID = getIntent().getStringExtra("driverSID");
            String selectedDriverVID = getIntent().getStringExtra("vID");
            selectedDriverAID = getIntent().getStringExtra("aID");

            RestCalls r = new RestCalls();
            String driverInfo = r.retrieveUser(DriverInfoActivity.this, selectedDriverSID);
            String vehicleInfo = r.retrieveVehicle(DriverInfoActivity.this, selectedDriverVID);
            String profileInfo = driverInfo + "," + vehicleInfo;
            return profileInfo.split(",");
        }

        @Override
        protected void onPostExecute(String[] driverAndVehicleDetails) {
            // Populate the UI elements with the retrieved data
            nameTextView.setText(driverAndVehicleDetails[0]);
            averageRatingTextView.setText(driverAndVehicleDetails[1]);
            makeAndModelTextView.setText(driverAndVehicleDetails[3] + " " + driverAndVehicleDetails[4]);
            registrationNumberTextView.setText(driverAndVehicleDetails[5]);
        }
    }
}
